package helpers;

import org.openqa.selenium.WebDriver;

public class LocalDriverManager
{
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
	private static ThreadLocal<SessionInfo> sessionInfo = new ThreadLocal<>();

    public static WebDriver getDriver() {
		WebDriver driver = webDriver.get();
		assert driver != null: "Driver was null, did you call 'setupDriver(sessionInfo.gridHub, sessionInfo.capabilities)'?";
        return driver;
    }

	public static SessionInfo getSessionInfo()
	{
		return sessionInfo.get();
	}

    public static void setWebDriver(WebDriver driver, SessionInfo sessionInfo) {
		LocalDriverManager.sessionInfo.set(sessionInfo);
        webDriver.set(driver);
    }
}