package helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class GPAHelper
{
	private final WebDriver driver;
	
	public GPAHelper(WebDriver driver)
	{
		this.driver = driver;
	}

	public GPAHelper()
	{
		driver = LocalDriverManager.getDriver();
	}

	public void assertString(String actual, String expected)
	{
		assert actual.equals(expected) :
				"\nActual   = '" + actual + "'" +
				"\nExpected = '" + expected + "'\n";
	}

	public void assertUrl(String url)
	{
		String currentUrl = driver.getCurrentUrl();
		assertString(currentUrl, url);
	}

	public GPAHelper clearInputText(By by)
	{
		clickElement(by);

		driver.findElement(by).clear();

		assert getValue(by).isEmpty() : "Value for '" + by + "' not empty. Value found: '" + getValue(by) + "'";

		return this;
	}

	public GPAHelper clickElement(By by)
	{
		wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.className("gw-modal-backdrop")));
		driver.findElement(by).click();
		return this;
	}

	public boolean isDisplayed(By by)
	{
		List<WebElement> elements = driver.findElements(by);
		try
		{
			return !elements.isEmpty() && elements.get(0).isDisplayed();
		}
		catch(StaleElementReferenceException e)
		{
			return false;
		}
	}

	public String getAttribute(By by, String attribute)
	{
		return driver.findElement(by).getAttribute(attribute);
	}

	public String getCurrentUrl()
	{
		return driver.getCurrentUrl();
	}

	public WebElement getElement(By by)
	{
		return driver.findElement(by);
	}

	public List<WebElement> getElements(By by)
	{
		return driver.findElements(by);
	}

	public String getText(By by)
	{
		return driver.findElement(by).getText();
	}

	public String getValue(By by)
	{
		return getAttribute(by, "value");
	}

	public GPAHelper setText(By by, String text)
	{
		return setText(by, text, "");
	}

	public GPAHelper setText(By by, String text, String expectedText)
	{
		wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.className("gw-modal-backdrop")));
		clearInputText(by);
		driver.findElement(by).sendKeys(text);
		if(expectedText.isEmpty())
			assertString(getValue(by), text);
		else
			assertString(getValue(by), expectedText);

		return this;
	}

	public GPAHelper tab()
	{
		new Actions(driver).sendKeys(Keys.TAB).perform();
		return this;
	}

	public WebDriverWait wait(int seconds)
	{
		return wait(seconds, 500);
	}

	public WebDriverWait wait(int seconds, int poll)
	{
		return new WebDriverWait(driver, seconds, poll);
	}

	public GPAHelper waitForNoBackdrop()
	{
		wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.className("gw-modal-backdrop")));
		return this;
	}

	public class ModalHelper
	{
		By modalOk = By.xpath("//div[contains(@class,'gw-modal')]//button[contains(.,'OK')]"),
			modalCancel = By.xpath("//div[contains(@class,'gw-modal')]//button[contains(.,'Cancel')]"),
			modalClose = By.xpath("//div[contains(@class,'gw-modal')]//button[contains(.,'Close')]");

		public void clickOk()
		{
			clickModalButton(modalOk);
		}

		public void clickCancel()
		{
			clickModalButton(modalCancel);
		}

		public void clickClose()
		{
			clickModalButton(modalClose);
		}

		private void clickModalButton(By by)
		{
			GPAHelper.this.wait(30).until(ExpectedConditions.elementToBeClickable(by));
			driver.findElement(by).click();
		}
	}

	public class SelectHelper
	{
		public Select createSelectElement(By by)
		{
			return new Select(driver.findElement(by));
		}

		//TODO: Submit to Selenium that in Firefox and Chrome, getText on hidden option elements returns empty string rather than throw error and IE returns javascript error
		public String getSelectedOption(By by)
		{
			return createSelectElement(by)
					.getFirstSelectedOption()
					.getText();
		}

		public void assertSelectedOption(final By by, final String expectedOption)
		{
			GPAHelper.this.wait(3)
			.until(ExpectedConditions.refreshed(new ExpectedCondition<Boolean>()
			{
				String actualSelectedOption;
				@Override
				public Boolean apply(WebDriver input)
				{
					actualSelectedOption = getSelectedOption(by);
					return actualSelectedOption.equals(expectedOption);
				}

				@Override
				public String toString()
				{
					return String.format("option ('%s') to be selected in the element located by %s. Found: ('%s')", expectedOption, by, actualSelectedOption);
				}
			}));
		}

		public List<String> getAllOptions(By by)
		{
			List<WebElement> elements = createSelectElement(by).getOptions();
			List<String> options = new ArrayList<>();

			for(WebElement element : elements)
			{
				//TODO: Submit to Selenium that Firefox driver auto-trims text
				options.add(element.getText());
			}

			return options;
		}

		public void assertOptions(final By by, final List<String> expectedOptions)
		{
			GPAHelper.this.wait(5)
			.until(ExpectedConditions.refreshed(new ExpectedCondition<Boolean>()
			{
				List<String> actualOptions;

				@Override
				public Boolean apply(WebDriver d)
				{
					actualOptions = getAllOptions(by);
					return actualOptions.size() == expectedOptions.size() && actualOptions.equals(expectedOptions);
				}

				@Override
				public String toString()
				{
					return "Options to be present in select located at "  + by + "'" +
							"\nActual Options Size:   " + actualOptions.size() +
							"\nExpected Options Size: " + expectedOptions.size() +
							"\nActual Options:        " + actualOptions +
							"\nExpected Options:      " + expectedOptions + "\n";
				}
			}));
		}

		public void selectOption(final By by, final String option)
		{
			GPAHelper.this.wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.className("gw-modal-backdrop")));
			GPAHelper.this.wait(5)
			.until(ExpectedConditions.refreshed(new ExpectedCondition<Boolean>()
			{
				@Override
				public Boolean apply(WebDriver d)
				{
					return createSelectElement(by).getOptions().size() > 1;
				}

				@Override
				public String toString()
				{
					return "options to appear '"+ option +"'. Drop down contains: " + getAllOptions(by);
				}
			}));

			try
			{
				createSelectElement(by).selectByVisibleText(option);
			}
			catch (NoSuchElementException e)
			{
				List<WebElement> options = createSelectElement(by).getOptions();
				StringBuilder sb = new StringBuilder();
				for (WebElement opt : options)
				{
					sb.append("'").append(opt.getText()).append("'\n");
				}

				throw new NoSuchElementException("ERROR: Could not find option = '" + option + "' for field: " + by
						+ "\nNumber of Options found: '" + options.size() + "'\nOptions Found:\n" + sb.toString());
			}
			String selectedOption = getSelectedOption(by);
			assert selectedOption.equals(option) : "Wrong Option Selected.\nExpected: '" + option + "'\nActual: '" + selectedOption + "'";
		}
	}
}