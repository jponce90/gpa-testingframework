package pageobjects;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;

public abstract class GWRadioBinary
{
	protected By by;
	protected WebDriver driver;
	protected GPAHelper gh;

	public GWRadioBinary(By by)
	{
		this.by = by;
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
	}

	//Method below returns True if left option is selected otherwise returns False
	public boolean getRadioLeftOrRight()
	{
		By valueLeft = new ByChained(by, By.cssSelector("input:first-of-type"));
		return Boolean.parseBoolean(gh.getAttribute(valueLeft, "aria-checked"));
	}

	public void toggleRadio()
	{
		By radiosBinary = By.cssSelector(".gw-radios-binary .gw-labels");
		gh.clickElement(new ByChained(by, radiosBinary));
	}

	protected void clickTitle(By title)
	{
		By titleChained = new ByChained(by, title);
		gh.waitForNoBackdrop();
		gh.clickElement(titleChained);
	}

	protected void setWithin1000ftBase()
	{
		By title = By.cssSelector("[title='Within 1000ft']");
		clickTitle(title);
		if(!getRadioLeftOrRight())
			clickTitle(title);
	}

	protected void setOver1000ftBase()
	{
		By title = By.cssSelector("[title='Over 1000ft']");
		clickTitle(title);
		if(getRadioLeftOrRight())
			clickTitle(title);
	}
}