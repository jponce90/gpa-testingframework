package pageobjects.ALHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.FLHO3.FLHO3SinglePageApplication;
import pageobjects.PossibleAccountMatches;

public class ALHO3
{
	public ALHO3SinglePageQuote quote;
	public ALHO3SinglePageApplication app;
	public PossibleAccountMatches possibleAccountMatches;

	public ALHO3()
	{
		quote = new ALHO3SinglePageQuote();
		app = new ALHO3SinglePageApplication();
		possibleAccountMatches = new PossibleAccountMatches();
	}

	public ALHO3SinglePageApplication continueAsNewCustomer()
	{
		possibleAccountMatches.clickContinueAsNewCustomer();
		new GPAHelper(LocalDriverManager.getDriver()).wait(30).until(ExpectedConditions.elementToBeClickable(app.applicantInfo.sectionBaseBy.sectionHeader));
		return app;
	}
}