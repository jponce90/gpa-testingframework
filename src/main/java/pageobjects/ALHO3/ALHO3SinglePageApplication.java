package pageobjects.ALHO3;

import pageobjects.ApplicationSectionBases.*;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class ALHO3SinglePageApplication
{
	public ALHO3AppApplicantInfo applicantInfo;
	public ALHO3AppHomeDetails homeDetails;
	public ALHO3AppWindMitigation windMitigation;
	public ALHO3AppCoverages coverages;
	public ALHO3AppQualification qualification;
	public ALHO3AppDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;
	public LossHistoryBase lossHistory;
	public BindPolicyBase bindPolicy;

	public ALHO3SinglePageApplication()
	{
		applicantInfo = new ALHO3AppApplicantInfo();
		homeDetails = new ALHO3AppHomeDetails();
		windMitigation = new ALHO3AppWindMitigation();
		coverages = new ALHO3AppCoverages();
		qualification = new ALHO3AppQualification();
		dwellingProtectionAndSurcharges = new ALHO3AppDwellingProtectionDiscountsAndSurcharges();
		lossHistory = new LossHistoryBase();
		bindPolicy = new BindPolicyBase();
	}
	/**The following classes hold methods that only affect fields
	 * that appear in the applications for ALHO3 exclusively*/

	public class ALHO3AppApplicantInfo extends ApplicationApplicantInfoBase<ALHO3AppApplicantInfo>
	{
		public ALHO3AppApplicantInfo showSection()
		{
			showSectionBase();
			return this;
		}
	}

	public class ALHO3AppQualification extends ApplicationQualificationBase<ALHO3AppQualification>
	{
		public ALHO3AppQualification showSection()
		{
			showSectionQualification();
			return this;
		}
	}

	public class ALHO3AppHomeDetails extends ApplicationHomeDetailsBase<ALHO3AppHomeDetails>
	{

		public ALHO3AppHomeDetails showSection()
		{
			super.showSectionHomeDetails();
			return this;
		}

		public ALHO3AppHomeDetails setAppHomeDetailFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			/**
			 * Show Home Details Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean inTheWindpool = hashMap.get("HomeDetailsInthewindpool").equals("Yes"),
					swimmingPool = hashMap.get("HomeDetailsIsthereaswimmingpool").equals("Yes"),
					secondaryHeating = hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"),
					animal = hashMap.get("HomeDetailsAnyanimalorexoticpets").equals("Yes");
			String wiringType = hashMap.get("HomeDetailsWiring"),
					wiringDescription = hashMap.get("HomeDetailsWiringtypedescription");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			selectValuationTypeBase(hashMap.get("HomeDetailsValuationType"))
			.setReplacementCost(hashMap.get("HomeDetailsEstimatedReplacementCost"))
			.setDistanceToFireStation(hashMap.get("HomeDetailsDistancetoFireStation"))
			.selectLocationType(hashMap.get("HomeDetailsLocationType"))
			.inTheWindPool.clickYesOrNo(inTheWindpool);
			if(hashMap.get("HomeDetailsDistancetoCoast").equals("Within 1000 ft"))
				distanceToCoast.setWithin1000ft();
			else
				distanceToCoast.setOver1000ft();
			//TODO set Purchase Date
			//TODO set Purchase Price
			//TODO market value
			occupiedDaily.clickYesOrNo(hashMap.get("HomeDetailsOccupiedDaily").equals("Yes"));
			setWeeksRentedAnnually(hashMap.get("HomeDetailsWeeksrentedannually"))
			.selectMinimumRentalIncrement(hashMap.get("HomeDetailsMinimumRentalIncrement"));
			underContractWithRentalManagement.clickYesOrNo(hashMap.get("HomeDetailsUndercontractwithrentalmanagement").equals("Yes"));
			this.swimmingPool.clickYesOrNo(swimmingPool);
			if(swimmingPool)
			{
				selectPoolLocation(hashMap.get("HomeDetailsPoolLocation"))
				.poolFenced.clickYesOrNo(hashMap.get("HomeDetailsIsthepoolFenced").equals("Yes"));
				divingBoard.clickYesOrNo(hashMap.get("HomeDetailsIsthereadivingboard").equals("Yes"));
				poolSlide.clickYesOrNo(hashMap.get("HomeDetailsIsthereapoolslide").equals("Yes"));
			}
			trampoline.clickYesOrNo(hashMap.get("HomeDetailsIsthereatrampoline").equals("Yes"));
			animals.clickYesOrNo(animal);
			if(animal)
			{
				//TODO fill out animal information
			}

			golfCarts.clickYesOrNo(hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"));
			recreationVehicles.clickYesOrNo(hashMap.get("HomeDetailsAnyownedrecreationvehicles").equals("Yes"));
			selectHousekeepingCondition(hashMap.get("HomeDetailsHousekeepingcondition"));
			selectNumberOfUnits(hashMap.get("HomeDetailsNumberofUnits"));
			selectUnitInFireWalls(hashMap.get("HomeDetailsUnitsinFireWalls"));
			setNumberOfStories(hashMap.get("HomeDetailsNumberofStories"));
			setSquareFootage(hashMap.get("HomeDetailsSquareFootage"));
			selectFoundationType(hashMap.get("HomeDetailsFoundationType"));
			selectPrimaryHeating(hashMap.get("HomeDetailsPrimaryHeating"));
			secondaryHeatingSystem.clickYesOrNo(secondaryHeating);
			selectPlumbing(hashMap.get("HomeDetailsPlumbing"))
			.setPlumbingYear(hashMap.get("HomeDetailsPlumbingyear"))
			.setWaterHeaterYear(hashMap.get("HomeDetailsWaterHeaterYear"))
			.selectWiring(wiringType);

			if(wiringType.equals("Other") && !wiringDescription.contains("blank"))
				setWiringDescription(wiringDescription);

			selectElectricalSystem(hashMap.get("HomeDetailsElectricalSystem"))
			.selectConditionOfRoof(hashMap.get("HomeDetailsConditionofRoof"))
			.screenEnclosureOnPremises.clickYesOrNo(hashMap.get("HomeDetailsIsthereaScreenenclosureonpremises").equals("Yes"));
			plumbingSystemKnownLeaks.clickYesOrNo(hashMap.get("HomeDetailsDoestheplumbingsystemhaveanyknownleaks").equals("Yes"));

			return this;
		}

		public ALHO3AppHomeDetails selectValuationType(String valuationMethod)
		{
			return selectValuationTypeBase(valuationMethod);
		}
	}

	public class ALHO3AppWindMitigation extends ApplicationWindMitigationBase<ALHO3AppWindMitigation>
	{
	}

	public class ALHO3AppCoverages extends CoveragesBase<ALHO3AppCoverages>
	{
	}

	public class ALHO3AppDwellingProtectionDiscountsAndSurcharges extends ApplicationDwellingProtectionDiscountsAndSurchargesBase<ALHO3AppDwellingProtectionDiscountsAndSurcharges>
	{
		public GWRadioBinaryYesNo burglarBars = super.burglarBarsBase;
		public ALHO3AppDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public ALHO3AppDwellingProtectionDiscountsAndSurcharges setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(LinkedHashMap hashMap)
		{
			/**
			 * Show Dwelling Protection Discounts And Surcharges Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean burglarBars = hashMap.get("Dwellingprotection,discounts&surchargesArethereanyburglarbarsonthewindows/doors").equals("Yes");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			lockedFence.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesIstherealockedfence").equals("Yes"));
			this.burglarBars.clickYesOrNo(burglarBars);
			if(burglarBars)
				safetyLatches.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesAretheresafetylatches").equals("Yes"));
			fireExtinguishers.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesOneormorefireextinguishersinthehome").equals("Yes"));
			deadbolts.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesDeadbolts").equals("Yes"));
			residenceVisibleToNeighbours.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesResidenceVisibletoNeighbors").equals("Yes"));

			return this;
		}
	}
}