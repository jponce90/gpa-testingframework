package pageobjects.ALHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.WebDriver;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.QuoteSectionBases.QuoteDwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.QuoteSectionBases.QuoteHomeDetailsBase;
import pageobjects.QuoteSectionBases.QuoteWindMitigationBase;
import pageobjects.SinglePageQuoteBase;

import java.util.LinkedHashMap;

public class ALHO3SinglePageQuote extends SinglePageQuoteBase
{
	private WebDriver driver;
	private GPAHelper gh;
	public ALHO3QuoteHomeDetails homeDetails;
	public ALHO3QuoteWindMitigation windMitigation;
	public ALHO3QuoteDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;
	public ALHO3QuoteCoverages coverages;

	public ALHO3SinglePageQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		homeDetails = new ALHO3QuoteHomeDetails();
		windMitigation = new ALHO3QuoteWindMitigation();
		dwellingProtectionAndSurcharges = new ALHO3QuoteDwellingProtectionDiscountsAndSurcharges();
		coverages = new ALHO3QuoteCoverages();
	}

	public class ALHO3QuoteHomeDetails extends QuoteHomeDetailsBase<ALHO3QuoteHomeDetails>
	{
		public ALHO3QuoteHomeDetails showSection()
		{
			showSectionHomeDetails();
			return this;
		}

		public ALHO3QuoteHomeDetails setHomeDetailsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection();

			setHomeDetailsFromHashMapBase(hashMap);
			setDwellingLimit(hashMap.get("HomeDetailsDwellingLimit"))
			.selectConstructionType(hashMap.get("HomeDetailsConstructionType"));
			if(hashMap.get("HomeDetailsDistancetoFireHydrant").equals("Within 1000 ft"))
				distanceToFireHydrant.setWithin1000ft();
			else
				distanceToFireHydrant.setOver1000ft();
			selectResidenceType(hashMap.get("HomeDetailsResidenceType"))
			.selectUsageType(hashMap.get("HomeDetailsUsageType"))
			.selectHowIsDwellingOccupied(hashMap.get("HomeDetailsHowisdwellingoccupied"));
			return this;
		}

		public ALHO3QuoteHomeDetails setDwellingLimit(String dwellingLimit)
		{
			setDwellingLimitBase(dwellingLimit);
			return this;
		}

		public String getDwellingLimit()
		{
			return super.getDwellingLimit();
		}
	}

	public class ALHO3QuoteWindMitigation extends QuoteWindMitigationBase<ALHO3QuoteWindMitigation>
	{
		public ALHO3QuoteWindMitigation showSection()
		{
			super.showSectionWindMitigation();
			return this;
		}

		public ALHO3QuoteWindMitigation setWindMitigationFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			String discountType = hashMap.get("WindMitigationDiscountType").split("[ ]{0,1};[ ]{0,1}")[0];
			showSection()

			.setRoofYear(hashMap.get("WindMitigationRoofYear"))
			.selectRoofType(hashMap.get("WindMitigationRoofType"))
			.selectRoofShape(hashMap.get("WindMitigationRoofShape"));
			if(!hashMap.get("WindMitigationOpeningProtection").contains("not"))
				selectOpeningProtection(hashMap.get("WindMitigationOpeningProtection"));
			selectDiscountType(discountType);
			if(discountType.contains("Fortified Home"))
				selectFortifiedHome(hashMap.get("WindMitigationDiscountType").split("[ ]{0,1};[ ]{0,1}")[1]);
			return this;
		}

		public ALHO3QuoteWindMitigation selectFortifiedHome(String fortifiedHome)
		{
			selectFortifiedHomeBase(fortifiedHome);
			return this;
		}

		public ALHO3QuoteWindMitigation selectDiscountType(String discountType)
		{
			super.selectDiscountType(discountType);
			return this;
		}

		public String getDiscountType()
		{
			return super.getDiscountType();
		}
	}

	public class ALHO3QuoteCoverages extends CoveragesBase<ALHO3QuoteCoverages>
	{
		public ALHO3SinglePageApplication continueToApp()
		{
			super.clickContinueToApplication();
			return new ALHO3SinglePageApplication();
		}

		public ALHO3QuoteCoverages clickMoreCoverage()
		{
			clickMoreCoverageBase();
			return this;
		}

		public ALHO3QuoteCoverages clickMostPopular()
		{
			clickMostPopularBase();
			return this;
		}

		public ALHO3QuoteCoverages clickLessCoverage()
		{
			clickLessCoverageBase();
			return this;
		}
	}

	public class ALHO3QuoteDwellingProtectionDiscountsAndSurcharges extends QuoteDwellingProtectionDiscountsAndSurchargesBase<ALHO3QuoteDwellingProtectionDiscountsAndSurcharges>
	{
		public ALHO3QuoteDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public ALHO3QuoteDwellingProtectionDiscountsAndSurcharges setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean fireAlarm = hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";")[0].equals("Yes"),
					fireSprinklers = hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";")[0].equals("Yes");

			setDwellingProtectionDiscountsAndSurchargesFromHashMapBase(hashMap);

			this.fireAlarm.clickYesOrNo(fireAlarm);
			if(fireAlarm)
				selectFireAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";[ ]{0,1}")[1]);
			this.smokeAlarms.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSmokeAlarms").equals("Yes"));
			this.fireSprinklers.clickYesOrNo(fireSprinklers);
			if(fireSprinklers)
				selectFireSprinklersType("Full");
				//TODO input just says 'enter Sprinkler type'
//				selectFireSprinklersType(hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";[ ]{0,1}")[1]);
			return this;
		}
	}
}