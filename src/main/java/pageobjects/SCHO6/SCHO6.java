package pageobjects.SCHO6;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.PossibleAccountMatches;
import pageobjects.SideBar;

public class SCHO6
{
	public SCHO6SinglePageQuote quote;
	public SCHO6SinglePageApplication app;
	public SideBar sideBar;
	private PossibleAccountMatches possibleAccountMatches;
	public SCHO6()
	{
		quote = new SCHO6SinglePageQuote();
		app = new SCHO6SinglePageApplication();
		possibleAccountMatches = new PossibleAccountMatches();
		sideBar = new SideBar();
	}

	public SCHO6SinglePageApplication continueAsNewCustomer()
	{
		possibleAccountMatches.clickContinueAsNewCustomer();
		new GPAHelper(LocalDriverManager.getDriver()).wait(30).until(ExpectedConditions.elementToBeClickable(app.applicantInfo.sectionBaseBy.sectionHeader));
		return app;
	}
}