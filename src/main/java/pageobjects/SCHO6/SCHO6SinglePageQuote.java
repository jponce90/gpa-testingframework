package pageobjects.SCHO6;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.WebDriver;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;
import pageobjects.QuoteSectionBases.QuoteDwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.QuoteSectionBases.QuoteHomeDetailsBase;
import pageobjects.QuoteSectionBases.QuoteWindMitigationBase;
import pageobjects.SinglePageQuoteBase;

import java.util.LinkedHashMap;

public class SCHO6SinglePageQuote extends SinglePageQuoteBase
{
	private WebDriver driver;
	private GPAHelper gh;
	public SCHO6QuoteHomeDetails homeDetails;
	public SCHO6QuoteWindMitigation windMitigation;
	public SCHO6QuoteDwellingProtectionDiscountsAndSurcharges dwellingProtectionDiscountsAndSurcharges;
	public SCHO6QuoteCoverages coverages;

	public SCHO6SinglePageQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		homeDetails = new SCHO6QuoteHomeDetails();
		windMitigation = new SCHO6QuoteWindMitigation();
		coverages = new SCHO6QuoteCoverages();
		dwellingProtectionDiscountsAndSurcharges = new SCHO6QuoteDwellingProtectionDiscountsAndSurcharges();
	}

	public class SCHO6QuoteHomeDetails extends QuoteHomeDetailsBase<SCHO6QuoteHomeDetails>
	{
		public SCHO6QuoteHomeDetails showSection()
		{
			showSectionHomeDetails();
			return this;
		}

		public SCHO6QuoteHomeDetails setHomeDetailsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection();

			setHomeDetailsFromHashMapBase(hashMap);

			setDwellingLimit(hashMap.get("HomeDetailsDwellingLimit"))
			.setPersonalProperty(hashMap.get("HomeDetailsPersonalProperty"))
			.selectConstructionType(hashMap.get("HomeDetailsConstructionType"));
			if(hashMap.get("HomeDetailsDistancetoFireHydrant").equals("Within 1000 ft"))
				distanceToFireHydrant.setWithin1000ft();
			else
				distanceToFireHydrant.setOver1000ft();
			selectResidenceType(hashMap.get("HomeDetailsResidenceType"))
			.selectUsageType(hashMap.get("HomeDetailsUsageType"))
			.selectHowIsDwellingOccupied(hashMap.get("HomeDetailsHowisdwellingoccupied"));
			return this;
		}

		public SCHO6QuoteHomeDetails setDwellingLimit(String dwellingLimit)
		{
			return super.setDwellingLimit(dwellingLimit);
		}

		public String getDwellingLimit()
		{
			return super.getDwellingLimit();
		}

		public SCHO6QuoteHomeDetails setPersonalProperty(String personalProperty)
		{
			return super.setPersonalProperty(personalProperty);
		}

		public String getPersonalProperty()
		{
			return super.getPersonalProperty();
		}
	}

	public class SCHO6QuoteWindMitigation extends QuoteWindMitigationBase<SCHO6QuoteWindMitigation>
	{
		public GWRadioBinaryYesNo roofCoverBuildingCodeCompliant = roofCoverBuildingCodeCompliantBase,
									roofWallBuildingCodeCompliant = roofWallBuildingCodeCompliantBase,
									roofDeckBuildingCodeCompliant = roofDeckBuildingCodeCompliantBase,
									secondaryWaterResistance = secondaryWaterResistanceBase;

		public SCHO6QuoteWindMitigation showSection()
		{
			super.showSectionWindMitigation();
			return this;
		}

		public SCHO6QuoteWindMitigation setWindMitigationFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			setWindMitigationFromHashMapBase(hashMap);


			setRoofYear(hashMap.get("WindMitigationRoofYear"))
			.selectRoofType(hashMap.get("WindMitigationRoofType"))
			.selectRoofShape(hashMap.get("WindMitigationRoofShape"))
			.roofCoverBuildingCodeCompliant.clickYesOrNo(hashMap.get("WindMitigationIstheRoofCoverConstructionBuildingCodeCompliant").equals("Yes"));
			roofWallBuildingCodeCompliant.clickYesOrNo(hashMap.get("WindMitigationIstheroofwallconnectionbuildingcodecompliant").equals("Yes"));
			roofDeckBuildingCodeCompliant.clickYesOrNo(hashMap.get("WindMitigationIstheroofdeckattachedbuildingcodecompliant").equals("Yes"));
			secondaryWaterResistance.clickYesOrNo(hashMap.get("WindMitigationSecondaryWaterResistance").equals("Yes"));
			if(!hashMap.get("WindMitigationOpeningProtection").contains("not"))
				selectOpeningProtection(hashMap.get("WindMitigationOpeningProtection"));
			return this;
		}
	}

	public class SCHO6QuoteDwellingProtectionDiscountsAndSurcharges extends QuoteDwellingProtectionDiscountsAndSurchargesBase<SCHO6QuoteDwellingProtectionDiscountsAndSurcharges>
	{

		public SCHO6QuoteDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public SCHO6QuoteDwellingProtectionDiscountsAndSurcharges setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean fireAlarm = hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";")[0].equals("Yes"),
					fireSprinklers = hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";")[0].equals("Yes"),
					burglarAlarm = hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";")[0].equals("Yes");

			setDwellingProtectionDiscountsAndSurchargesFromHashMapBase(hashMap);

			this.burglarAlarm.clickYesOrNo(burglarAlarm);
			if(burglarAlarm)
				selectBurglarAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";[ ]{0,1}")[1]);
			this.fireAlarm.clickYesOrNo(fireAlarm);
			if(fireAlarm)
				selectFireAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";[ ]{0,1}")[1]);
			this.smokeAlarms.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSmokeAlarms").equals("Yes"));
			this.fireSprinklers.clickYesOrNo(fireSprinklers);
			if(fireSprinklers)
				selectFireSprinklersType("Full");
				//TODO input just says 'enter Sprinkler type'
//				selectFireSprinklersType(hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";[ ]{0,1}")[1]);
			return this;
		}
	}

	public class SCHO6QuoteCoverages extends CoveragesBase<SCHO6QuoteCoverages>
	{
		public SCHO6SinglePageApplication continueToApp()
		{
			super.clickContinueToApplication();
			return new SCHO6SinglePageApplication();
		}

		public SCHO6QuoteCoverages clickMoreCoverage()
		{
			super.clickMoreCoverageBase();
			return this;
		}

		public SCHO6QuoteCoverages clickMostPopular()
		{
			super.clickMostPopularBase();
			return this;
		}

		public SCHO6QuoteCoverages clickLessCoverage()
		{
			super.clickLessCoverageBase();
			return this;
		}
	}
}