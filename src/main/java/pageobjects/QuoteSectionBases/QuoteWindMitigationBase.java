package pageobjects.QuoteSectionBases;

import pageobjects.BaseSections.WindMitigationBase;

public abstract class QuoteWindMitigationBase<W extends QuoteWindMitigationBase> extends WindMitigationBase
{
	public W setRoofYear(String roofYear)
	{
		setRoofYearBase(roofYear);
		return (W)this;
	}

	public String getRoofYear()
	{
		return super.getRoofYear();
	}

	public W selectRoofType(String roofType)
	{
		selectRoofTypeBase(roofType);
		return (W)this;
	}

	public String getRoofType()
	{
		return super.getRoofType();
	}

	public W selectRoofShape(String roofShape)
	{
		selectRoofShapeBase(roofShape);
		return (W)this;
	}

	public String getRoofShape()
	{
		return super.getRoofShape();
	}

	public W selectRoofDeck(String roofDeck)
	{
		selectRoofDeckBase(roofDeck);
		return (W)this;
	}

	public String getRoofDeck()
	{
		return super.getRoofDeck();
	}

	public W selectRoofCover(String roofCover)
	{
		selectRoofCoverBase(roofCover);
		return (W)this;
	}

	public String getRoofCover()
	{
		return super.getRoofCover();
	}

	public W selectRoofWallConnection(String roofWallConnection)
	{
		selectRoofWallConnectionBase(roofWallConnection);
		return (W)this;
	}

	public String getRoofWallConnection()
	{
		return super.getRoofWallConnection();
	}

	public W selectOpeningProtection(String openingProtection)
	{
		selectOpeningProtectionBase(openingProtection);
		return (W)this;
	}

	public String getOpeningProtection()
	{
		return super.getOpeningProtection();
	}

	protected W selectDiscountType(String discountType)
	{
		selectDiscountTypeBase(discountType);
		return (W)this;
	}

	protected String getDiscountType()
	{
		return super.getDiscountType();
	}
}