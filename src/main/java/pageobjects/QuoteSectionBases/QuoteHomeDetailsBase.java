package pageobjects.QuoteSectionBases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.BaseSections.HomeDetailsBase;

public abstract class QuoteHomeDetailsBase<H extends QuoteHomeDetailsBase> extends HomeDetailsBase
{
	public final QuoteHomeDetailsBaseBy by = new QuoteHomeDetailsBaseBy();
	public final DistanceToFireHydrant distanceToFireHydrant = new DistanceToFireHydrant();

	public final class QuoteHomeDetailsBaseBy
	{
	}

	protected H setDwellingLimit(String dwellingLimit)
	{
		super.setDwellingLimitBase(dwellingLimit);
		return (H)this;
	}

	protected String getDwellingLimit()
	{
		return super.getDwellingLimitBase();
	}

	public H setYearBuilt(String yearBuilt)
	{
		setYearBuiltBase(yearBuilt);
		return (H)this;
	}

	protected H setPersonalProperty(String personalProperty)
	{
		setPersonalPropertyBase(personalProperty);
		return (H)this;
	}

	public H selectConstructionType(String constructionType)
	{
		selectConstructionTypeBase(constructionType);
		return (H)this;
	}

	public H selectResidenceType(String residenceType)
	{
		selectResidenceTypeBase(residenceType);
		return (H)this;
	}

	public H selectUsageType(String usageType)
	{
		selectUsageTypeBase(usageType);
		return (H)this;
	}

	public H selectHowIsDwellingOccupied(String howIsDwellingOccupied)
	{
		selectHowIsDwellingOccupiedBase(howIsDwellingOccupied);
		return (H)this;
	}
}