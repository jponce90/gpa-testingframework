package pageobjects.NCHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;
import pageobjects.QuoteSectionBases.QuoteDwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.QuoteSectionBases.QuoteHomeDetailsBase;
import pageobjects.QuoteSectionBases.QuoteWindMitigationBase;
import pageobjects.SinglePageQuoteBase;

import java.util.LinkedHashMap;

public class NCHO3SinglePageQuote extends SinglePageQuoteBase
{
	private WebDriver driver;
	private GPAHelper gh;
	public NCHO3QuoteHomeDetails homeDetails;
	public NCHO3QuoteWindMitigation windMitigation;
	public NCHO3QuoteCoverages coverages;
	public NCHO3QuoteDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;

	public NCHO3SinglePageQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		homeDetails = new NCHO3QuoteHomeDetails();
		windMitigation = new NCHO3QuoteWindMitigation();
		coverages = new NCHO3QuoteCoverages();
		dwellingProtectionAndSurcharges = new NCHO3QuoteDwellingProtectionDiscountsAndSurcharges();
	}

	public class NCHO3QuoteHomeDetails extends QuoteHomeDetailsBase<NCHO3QuoteHomeDetails>
	{
		public NCHO3QuoteHomeDetails showSection()
		{
			showSectionHomeDetails();
			return this;
		}

		public NCHO3QuoteHomeDetails setHomeDetailsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection();

			setHomeDetailsFromHashMapBase(hashMap);
			selectConstructionType(hashMap.get("HomeDetailsConstructionType"));
			if(hashMap.get("HomeDetailsDistancetoFireHydrant").equals("Within 1000 ft"))
				distanceToFireHydrant.setWithin1000ft();
			else
				distanceToFireHydrant.setOver1000ft();
			selectResidenceType(hashMap.get("HomeDetailsResidenceType"))
			.selectUsageType(hashMap.get("HomeDetailsUsageType"))
			.selectHowIsDwellingOccupied(hashMap.get("HomeDetailsHowisdwellingoccupied"));

			return this;
		}
	}


	public class NCHO3QuoteWindMitigation extends QuoteWindMitigationBase<NCHO3QuoteWindMitigation>
	{
		public NCHO3QuoteWindMitigation showSection()
		{
			super.showSectionWindMitigation();
			return this;
		}

		public NCHO3QuoteWindMitigation setWindMitigationFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			String discountType = hashMap.get("WindMitigationDiscountType").split("[ ]{0,1};[ ]{0,1}")[0];

			showSection()

			.setRoofYear(hashMap.get("WindMitigationRoofYear"))
			.selectRoofType(hashMap.get("WindMitigationRoofType"))
			.selectRoofShape(hashMap.get("WindMitigationRoofShape"));
			if(!hashMap.get("WindMitigationOpeningProtection").contains("not"))
				selectOpeningProtection(hashMap.get("WindMitigationOpeningProtection"));
			selectDiscountType(discountType);
			if(discountType.contains("Fortified Home"))
				selectFortifiedHome(hashMap.get("WindMitigationDiscountType").split("[ ]{0,1};[ ]{0,1}")[1]);
			return this;
		}

		public NCHO3QuoteWindMitigation selectFortifiedHome(String fortifiedHome)
		{
			selectFortifiedHomeBase(fortifiedHome);
			return this;
		}
	}

	public class NCHO3QuoteCoverages extends CoveragesBase<NCHO3QuoteCoverages>
	{
		NCHO3QuoteCoveragesBy by = new NCHO3QuoteCoveragesBy();

		public class NCHO3QuoteCoveragesBy
		{
			public By dwellingLimit = By.xpath("//tbody[contains(.,'Dwelling')]//tr[contains(.,'Limit')]//input"),
						calculateQuote = By.xpath("//div[contains(@class, 'gw-start-policy')]//button[contains(.,'Calculate quote')]");
		}

		public NCHO3SinglePageApplication continueToApp()
		{
			super.clickContinueToApplication();
			return new NCHO3SinglePageApplication();
		}

		public NCHO3QuoteCoverages windOnlyModalClickOk()
		{
			mh.clickOk();
			return this;
		}

		public NCHO3QuoteCoverages setDwellingLimit(String limit)
		{
			gh.setText(by.dwellingLimit, limit);
			gh.tab();
			return this;
		}

		public NCHO3QuoteCoverages clickCalculateQuote()
		{
			gh.wait(10).until(ExpectedConditions.elementToBeClickable(by.calculateQuote));
			gh.clickElement(by.calculateQuote);
			return this;
		}

		public NCHO3QuoteCoverages calculateQuote()
		{
			clickCalculateQuote();
			gh.wait(30).until(ExpectedConditions.elementToBeClickable(super.by.continueToApp));
			return this;
		}
	}

	public class NCHO3QuoteDwellingProtectionDiscountsAndSurcharges extends QuoteDwellingProtectionDiscountsAndSurchargesBase<NCHO3QuoteDwellingProtectionDiscountsAndSurcharges>
	{
		public GWRadioBinaryYesNo automaticSmokeDetectors = new GWRadioBinaryYesNo(By.cssSelector("[label='Automatic Smoke Detectors']"));

		public NCHO3QuoteDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public NCHO3QuoteDwellingProtectionDiscountsAndSurcharges setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean fireAlarm = hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";")[0].equals("Yes"),
					fireSprinklers = hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";")[0].equals("Yes"),
					burglarAlarm = hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";")[0].equals("Yes");

			showSection();

			seniorCitizenDiscountBase.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSeniorCitizenDiscount").equals("Yes"));
			this.burglarAlarm.clickYesOrNo(burglarAlarm);
			if(burglarAlarm)
				selectBurglarAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";[ ]{0,1}")[1]);
			this.guardedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGuardedCommunity").equals("Yes"));
			this.gatedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGatedCommunity").equals("Yes"));
			this.fireAlarm.clickYesOrNo(fireAlarm);
			if(fireAlarm)
				selectFireAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";[ ]{0,1}")[1]);
			automaticSmokeDetectors.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesAutomaticSmokeDetectors").equals("Yes"));
			this.smokeAlarms.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSmokeAlarms").equals("Yes"));
			this.fireSprinklers.clickYesOrNo(fireSprinklers);
			if(fireSprinklers)
				selectFireSprinklersType("Full");
				//TODO input just says 'enter Sprinkler type'
//				selectFireSprinklersType(hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";[ ]{0,1}")[1]);
			return this;
		}
	}
}