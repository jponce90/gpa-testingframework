package pageobjects.NCHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.FLHO3.FLHO3SinglePageApplication;
import pageobjects.PossibleAccountMatches;

public class NCHO3
{
	public NCHO3SinglePageQuote quote;
	public NCHO3SinglePageApplication app;
	public PossibleAccountMatches possibleAccountMatches;

	public NCHO3()
	{
		quote = new NCHO3SinglePageQuote();
		app = new NCHO3SinglePageApplication();
		possibleAccountMatches = new PossibleAccountMatches();
	}

	public NCHO3SinglePageApplication continueAsNewCustomer()
	{
		possibleAccountMatches.clickContinueAsNewCustomer();
		new GPAHelper(LocalDriverManager.getDriver()).wait(30).until(ExpectedConditions.elementToBeClickable(app.applicantInfo.sectionBaseBy.sectionHeader));
		return app;
	}
}