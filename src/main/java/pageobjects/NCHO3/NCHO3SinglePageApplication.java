package pageobjects.NCHO3;

import pageobjects.ApplicationSectionBases.*;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class NCHO3SinglePageApplication
{
	public NCHO3AppApplicantInfo applicantInfo;
	public NCHO3AppHomeDetails homeDetails;
	public NCHO3AppWindMitigation windMitigation;
	public NCHO3AppCoverages coverages;
	public NCHO3AppQualification qualification;
	public NCHO3AppDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;
	public LossHistoryBase lossHistory;
	public BindPolicyBase bindPolicy;

	public NCHO3SinglePageApplication()
	{
		applicantInfo = new NCHO3AppApplicantInfo();
		homeDetails = new NCHO3AppHomeDetails();
		windMitigation = new NCHO3AppWindMitigation();
		coverages = new NCHO3AppCoverages();
		qualification = new NCHO3AppQualification();
		dwellingProtectionAndSurcharges = new NCHO3AppDwellingProtectionDiscountsAndSurcharges();
		lossHistory = new LossHistoryBase();
		bindPolicy = new BindPolicyBase();
	}
	/**The following classes hold methods that only affect fields
	 * that appear in the applications for NCHO3 exclusively*/

	public class NCHO3AppApplicantInfo extends ApplicationApplicantInfoBase<NCHO3AppApplicantInfo>
	{
		public NCHO3AppApplicantInfo showSection()
		{
			showSectionBase();
			return this;
		}

		public NCHO3AppApplicantInfo setAppApplicantInfoFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean mailingAddressSameAsLocation = hashMap.get("PolicyApplication&LocationInfoMailingaddressthesameaslocationaddress").equals("Yes");

			showSection();

			setDateOfBirth("04/18/1990")
			.setSSN("123-12-1234")
			.setOccupation("Welder")
			.mailingAddressSameAsLocationAddress.clickYesOrNo(mailingAddressSameAsLocation);
			if(!mailingAddressSameAsLocation)
				setMailingAddress1(hashMap.get("PolicyApplication&LocationInfoMailingaddress1"))
				.setMailingAddressCity(hashMap.get("PolicyApplication&LocationInfoCity"))
				.setZip(hashMap.get("PolicyApplication&LocationInfoZipCode"))
				.selectState(hashMap.get("PolicyApplication&LocationInfoState"))
				.selectAddressType(hashMap.get("PolicyApplication&LocationInfoAddressType"));

			String[] phoneInfo;

			if(!hashMap.get("PolicyApplication&LocationInfoHomePhone").contains("blank"))
			{
				phoneInfo = hashMap.get("PolicyApplication&LocationInfoHomePhone").split(" - ");
				setHomePhone(phoneInfo[0]);
				if(phoneInfo.length > 1 && phoneInfo[1].contains("primary"))
					setHomePhoneAsPrimary();
			}
			if(!hashMap.get("PolicyApplication&LocationInfoWorkPhone").contains("blank"))
			{
				phoneInfo = hashMap.get("PolicyApplication&LocationInfoWorkPhone").split(" - ");
				setWorkPhone(phoneInfo[0]);
				if(phoneInfo.length > 1 && phoneInfo[1].contains("primary"))
					setWorkPhoneAsPrimary();
			}
			if(!hashMap.get("PolicyApplication&LocationInfoCell").contains("blank"))
			{
				phoneInfo = hashMap.get("PolicyApplication&LocationInfoCell").split(" - ");
				setCellPhone(phoneInfo[0]);
				if(phoneInfo.length > 1 && phoneInfo[1].contains("primary"))
					setCellPhoneAsPrimary();
			}
			return this;
		}
	}

	public class NCHO3AppQualification extends ApplicationQualificationBase<NCHO3AppQualification>
	{
		public NCHO3AppQualification showSection()
		{
			showSectionQualification();
			return this;
		}
	}

	public class NCHO3AppHomeDetails extends ApplicationHomeDetailsBase<NCHO3AppHomeDetails>
	{
		public NCHO3AppHomeDetails showSection()
		{
			super.showSectionHomeDetails();
			return this;
		}

		public NCHO3AppHomeDetails setAppHomeDetailFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			/**
			 * Show Home Details Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean inTheWindpool = hashMap.get("HomeDetailsInthewindpool").equals("Yes"),
					swimmingPool = hashMap.get("HomeDetailsIsthereaswimmingpool").equals("Yes"),
					secondaryHeating = hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"),
					animal = hashMap.get("HomeDetailsAnyanimalorexoticpets").equals("Yes");
			String wiringType = hashMap.get("HomeDetailsWiring"),
					wiringDescription = hashMap.get("HomeDetailsWiringtypedescription");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			selectValuationTypeBase(hashMap.get("HomeDetailsValuationType"))
			.setReplacementCost(hashMap.get("HomeDetailsEstimatedReplacementCost"))
			.setDistanceToFireStation(hashMap.get("HomeDetailsDistancetoFireStation"))
			.selectLocationType(hashMap.get("HomeDetailsLocationType"))
			.inTheWindPool.clickYesOrNo(inTheWindpool);
			if(hashMap.get("HomeDetailsDistancetoCoast").equals("Within 1000 ft"))
				distanceToCoast.setWithin1000ft();
			else
				distanceToCoast.setOver1000ft();
			//TODO set Purchase Date
			//TODO set Purchase Price
			//TODO market value
			occupiedDaily.clickYesOrNo(hashMap.get("HomeDetailsOccupiedDaily").equals("Yes"));
			setWeeksRentedAnnually(hashMap.get("HomeDetailsWeeksrentedannually"))
			.selectMinimumRentalIncrement(hashMap.get("HomeDetailsMinimumRentalIncrement"));
			underContractWithRentalManagement.clickYesOrNo(hashMap.get("HomeDetailsUndercontractwithrentalmanagement").equals("Yes"));
			this.swimmingPool.clickYesOrNo(swimmingPool);
			if(swimmingPool)
			{
				selectPoolLocation(hashMap.get("HomeDetailsPoolLocation"))
				.poolFenced.clickYesOrNo(hashMap.get("HomeDetailsIsthepoolFenced").equals("Yes"));
				divingBoard.clickYesOrNo(hashMap.get("HomeDetailsIsthereadivingboard").equals("Yes"));
				poolSlide.clickYesOrNo(hashMap.get("HomeDetailsIsthereapoolslide").equals("Yes"));
			}
			trampoline.clickYesOrNo(hashMap.get("HomeDetailsIsthereatrampoline").equals("Yes"));
			animals.clickYesOrNo(animal);
			if(animal)
			{
				//TODO fill out animal information
			}

			golfCarts.clickYesOrNo(hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"));
			recreationVehicles.clickYesOrNo(hashMap.get("HomeDetailsAnyownedrecreationvehicles").equals("Yes"));
			selectHousekeepingCondition(hashMap.get("HomeDetailsHousekeepingcondition"));
			selectNumberOfUnits(hashMap.get("HomeDetailsNumberofUnits"));
			selectUnitInFireWalls(hashMap.get("HomeDetailsUnitsinFireWalls"));
			setNumberOfStories(hashMap.get("HomeDetailsNumberofStories"));
			setSquareFootage(hashMap.get("HomeDetailsSquareFootage"));
			selectFoundationType(hashMap.get("HomeDetailsFoundationType"));
			selectPrimaryHeating(hashMap.get("HomeDetailsPrimaryHeating"));
			secondaryHeatingSystem.clickYesOrNo(secondaryHeating);
			selectPlumbing(hashMap.get("HomeDetailsPlumbing"))
			.setPlumbingYear(hashMap.get("HomeDetailsPlumbingyear"))
			.setWaterHeaterYear(hashMap.get("HomeDetailsWaterHeaterYear"))
			.selectWiring(wiringType);

			if(wiringType.equals("Other") && !wiringDescription.contains("blank"))
				setWiringDescription(wiringDescription);

			selectElectricalSystem(hashMap.get("HomeDetailsElectricalSystem"))
			.selectConditionOfRoof(hashMap.get("HomeDetailsConditionofRoof"))
			.screenEnclosureOnPremises.clickYesOrNo(hashMap.get("HomeDetailsIsthereascreenenclosureonpremises").equals("Yes"));
			plumbingSystemKnownLeaks.clickYesOrNo(hashMap.get("HomeDetailsDoestheplumbingsystemhaveanyknownleaks").equals("Yes"));

			return this;
		}

		public NCHO3AppHomeDetails selectValuationType(String valuationMethod)
		{
			return selectValuationTypeBase(valuationMethod);
		}
	}

	public class NCHO3AppWindMitigation extends ApplicationWindMitigationBase<NCHO3AppWindMitigation>
	{
	}

	public class NCHO3AppCoverages extends CoveragesBase<NCHO3AppCoverages>
	{
	}

	public class NCHO3AppDwellingProtectionDiscountsAndSurcharges extends ApplicationDwellingProtectionDiscountsAndSurchargesBase<NCHO3AppDwellingProtectionDiscountsAndSurcharges>
	{
		public GWRadioBinaryYesNo burglarBars = super.burglarBarsBase;
		public NCHO3AppDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public NCHO3AppDwellingProtectionDiscountsAndSurcharges setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(LinkedHashMap hashMap)
		{
			/**
			 * Show Dwelling Protection Discounts And Surcharges Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean burglarBars = hashMap.get("Dwellingprotection,discounts&surchargesArethereanyburglarbarsonthewindows/doors").equals("Yes");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			lockedFence.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesIstherealockedfence").equals("Yes"));
			this.burglarBars.clickYesOrNo(burglarBars);
			if(burglarBars)
				safetyLatches.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesAretheresafetylatches").equals("Yes"));
			fireExtinguishers.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesOneormorefireextinguishersinthehome").equals("Yes"));
			deadbolts.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesDeadbolts").equals("Yes"));
			residenceVisibleToNeighbours.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesResidenceVisibletoNeighbors").equals("Yes"));

			return this;
		}
	}
}