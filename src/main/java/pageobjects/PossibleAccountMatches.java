package pageobjects;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class PossibleAccountMatches
{
	WebDriver driver;
	GPAHelper gh;
	private PossibleAccountMatchesBy by = new PossibleAccountMatchesBy();

	public PossibleAccountMatches()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
	}

	public final class PossibleAccountMatchesBy
	{
		public final By continueAsNewCustomer = By.cssSelector("button[ng-click*='continueAsNewCustomer']");
		private By useThisAccount(int accountByIndex)
		{
			return By.cssSelector(".gw-table tr:nth-of-type("+ accountByIndex +") p[ng-if*='canUserCreateSubmission']");
		}
	}

	public void clickContinueAsNewCustomer()
	{
		gh.wait(30).until(ExpectedConditions.elementToBeClickable(by.continueAsNewCustomer));
		gh.clickElement(by.continueAsNewCustomer);
	}

	public void clickUseThisAccount(int index)
	{
		gh.clickElement(by.useThisAccount(index));
	}
}