package pageobjects.ApplicationSectionBases;

import org.openqa.selenium.By;
import pageobjects.BaseSections.HomeDetailsBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public abstract class ApplicationHomeDetailsBase<H extends ApplicationHomeDetailsBase> extends HomeDetailsBase
{
	protected ApplicationHomeDetailsBaseBy by = new ApplicationHomeDetailsBaseBy();
	public final DistanceToCoastRadio distanceToCoast = new DistanceToCoastRadio();
	public final DistanceToFireHydrant distanceToFireHydrant = new DistanceToFireHydrant();
	public final GWRadioBinaryYesNo screenEnclosureOnPremises = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a screen enclosure on premises?']")),
									plumbingSystemKnownLeaks = new GWRadioBinaryYesNo(By.cssSelector("[label='Does the plumbing system have known leaks?']")),
									inTheWindPool = new GWRadioBinaryYesNo(By.cssSelector("[label='In the windpool?']")),
									swimmingPool = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a swimming pool?']")),
									poolFenced = new GWRadioBinaryYesNo(By.cssSelector("[label='Is the pool fenced?']")),
									divingBoard = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a diving board?']")),
									poolSlide = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a pool slide?']")),
									trampoline = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a trampoline?']")),
									animals = new GWRadioBinaryYesNo(By.cssSelector("[label='Any animals or exotic pets?']")),
									golfCarts = new GWRadioBinaryYesNo(By.cssSelector("[label='Any owned golf carts?']")),
									secondaryHeatingSystem = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a secondary heating system?']")),
									recreationVehicles = new GWRadioBinaryYesNo(By.cssSelector("[label='Any owned recreational vehicles?']")),
									occupiedDaily = new GWRadioBinaryYesNo(By.cssSelector("[label='Occupied Daily?']"));
	protected final GWRadioBinaryYesNo underContractWithRentalManagement = new GWRadioBinaryYesNo(By.cssSelector("[label='Under contract with rental management?']"));

	public final class ApplicationHomeDetailsBaseBy
	{
		public final By valuationType = By.cssSelector("[label='Valuation Type'] select"),
						estimatedReplacementCost = By.cssSelector("[label='Estimated Replacement Cost'] input"),
						distanceToFireStation = By.cssSelector("[label='Distance to Fire Station (mi)'] input"),
						locationType = By.cssSelector("[label='Location Type'] select"),
						weeksRentedAnnually = By.cssSelector("[label='Weeks rented annually'] input"),
						minimumRentalIncrement = By.cssSelector("[label='Minimum Rental Increment'] select"),
						poolLocation = By.cssSelector("[label='Pool location'] select"),
						unitsInFireWalls = By.cssSelector("[label='Units in Fire Walls'] select"),
						numberOfStories = By.cssSelector("[label='Number of Stories'] input"),
						squareFootage = By.cssSelector("[label='Square Footage'] input"),
						foundationType = By.cssSelector("[label='Foundation Type'] select"),
						primaryHeating = By.cssSelector("[label='Primary Heating'] select"),
						plumbing = By.cssSelector("[label='Plumbing'] select"),
						plumbingYear = By.cssSelector("[label='Plumbing year'] input"),
						waterHeaterYear = By.cssSelector("[label='Water heater year'] input"),
						wiring = By.cssSelector("[label='Wiring'] select"),
						wiringDescription = By.cssSelector("[label='Wiring type description'] input"),
						electricalSystem = By.cssSelector("[label='Electrical System'] select"),
						unitFloor = By.cssSelector("[label='Unit Floor'] input"),
						numberOfUnits = By.cssSelector("[label='Number of Units'] select"),
						housekeepingCondition = By.cssSelector("[label='Housekeeping condition'] select"),
						conditionOfRoof = By.cssSelector("[label='Condition of Roof'] select");
	}

	protected void setHomeDetailsFromHashMapBase(LinkedHashMap<String, String> hashMap)
	{
		showSectionHomeDetails();

		setDistanceToFireStation(hashMap.get("HomeDetailsDistanceToFireStation"));
		setSquareFootage(hashMap.get("HomeDetailsSquareFootage"));
	}

	protected H selectValuationTypeBase(String valuationType)
	{
		ghs.selectOption(by.valuationType, valuationType);
		return (H)this;
	}

	public H setReplacementCost(String replacementCost)
	{
		gh.setText(by.estimatedReplacementCost, replacementCost);
		return (H)this;
	}

	public H setDistanceToFireStation(String distanceInMiles)
	{
		gh.setText(by.distanceToFireStation, distanceInMiles);
		return (H)this;
	}

	public H setWeeksRentedAnnually(String weeksRented)
	{
		gh.setText(by.weeksRentedAnnually, weeksRented);
		return (H)this;
	}

	public H selectLocationType(String locationType)
	{
		ghs.selectOption(by.locationType, locationType);
		return (H)this;
	}

	public H selectMinimumRentalIncrement(String minimumRentalIncrement)
	{
		ghs.selectOption(by.minimumRentalIncrement, minimumRentalIncrement);
		return (H)this;
	}

	public H selectPoolLocation(String poolLocation)
	{
		ghs.selectOption(by.poolLocation, poolLocation);
		return (H)this;
	}

	public H selectUnitInFireWalls(String units)
	{
		ghs.selectOption(by.unitsInFireWalls, units);
		return (H)this;
	}

	public H setNumberOfStories(String stories)
	{
		gh.setText(by.numberOfStories, stories);
		return (H)this;
	}

	public H setSquareFootage(String squareFootage)
	{
		gh.setText(by.squareFootage, squareFootage);
		return (H)this;
	}

	public H selectFoundationType(String foundationType)
	{
		ghs.selectOption(by.foundationType, foundationType);
		return (H)this;
	}

	public H selectPrimaryHeating(String primaryHeating)
	{
		ghs.selectOption(by.primaryHeating, primaryHeating);
		return (H)this;
	}

	public H selectPlumbing(String plumbing)
	{
		ghs.selectOption(by.plumbing, plumbing);
		return (H)this;
	}

	public H setPlumbingYear(String plumbingYear)
	{
		gh.setText(by.plumbingYear, plumbingYear);
		return (H)this;
	}

	public H setWaterHeaterYear(String waterHeaterYear)
	{
		gh.setText(by.waterHeaterYear, waterHeaterYear);
		return (H)this;
	}

	public H selectHousekeepingCondition(String housekeepingCondition)
	{
		ghs.selectOption(by.housekeepingCondition, housekeepingCondition);
		return (H)this;
	}

	public H selectNumberOfUnits(String numberOfUnits)
	{
		ghs.selectOption(by.numberOfUnits, numberOfUnits);
		return (H)this;
	}

	public H selectWiring(String wiring)
	{
		ghs.selectOption(by.wiring, wiring);
		return (H)this;
	}

	public H setWiringDescription(String description)
	{
		gh.setText(by.wiringDescription, description);
		return (H)this;
	}

	public String getYearBuiltReadOnly()
	{
		return gh.getText(super.by.yearBuiltReadOnly);
	}

	public String getConstructionTypeReadOnly()
	{
		return gh.getText(super.by.constructionTypeReadOnly);
	}

	public String getDistanceToFireHydrantReadOnly()
	{
		return gh.getText(super.by.constructionTypeReadOnly);
	}

	public H selectElectricalSystem(String electricalSystem)
	{
		ghs.selectOption(by.electricalSystem, electricalSystem);
		return (H)this;
	}

	protected H setUnitFloor(String unitFloor)
	{
		gh.setText(by.unitFloor, unitFloor);
		return (H)this;
	}

	public H selectConditionOfRoof(String conditionOfRoof)
	{
		ghs.selectOption(by.conditionOfRoof, conditionOfRoof);
		return (H)this;
	}
}