package pageobjects.ApplicationSectionBases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.BaseSections.SectionBase;

public class LossHistoryBase<L extends LossHistoryBase> extends SectionBase
{
	LossHistoryBaseBy by = new LossHistoryBaseBy();
	public final class LossHistoryBaseBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Loss History')]"),
						orderLossHistoryReport = By.cssSelector("button[ng-click*='orderLossHistory']"),
						priorLossReportLink = By.xpath("//span[contains(.,'PriorLossReport')]");
	}

	public L showSection()
	{
		gh.clickElement(by.sectionHeader);
		gh.wait(5).until(ExpectedConditions.elementToBeClickable(by.orderLossHistoryReport));
		return (L)this;
	}

	public L clickOrderLossHistoryReport()
	{
		gh.clickElement(by.orderLossHistoryReport);
		return (L)this;
	}

	public L orderLossReport()
	{
		clickOrderLossHistoryReport();
		gh.wait(30).until(ExpectedConditions.elementToBeClickable(by.priorLossReportLink));
		return (L)this;
	}
}