package pageobjects.ApplicationSectionBases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.BaseSections.SectionBase;

public class BindPolicyBase extends SectionBase
{
	private BindPolicyBy by = new BindPolicyBy();

	public final class BindPolicyBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Bind Policy')]"),
						bindConfirmation = By.xpath("//h4[contains(.,'Bind Confirmation')]"),
						okBindModal = By.cssSelector(".gw-modal-footer button[ng-click*='close']"),
						cancelBindModal = By.cssSelector(".gw-modal-footer button[ng-click*='dismiss']");
	}

	public void bindPolicy()
	{
		showSectionBase(by.sectionHeader, by.okBindModal);
		gh.wait(5).until(ExpectedConditions.elementToBeClickable(by.okBindModal));
		driver.findElement(by.okBindModal).click();
		gh.wait(90).until(ExpectedConditions.visibilityOfElementLocated(by.bindConfirmation));
	}
}