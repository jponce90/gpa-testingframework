package pageobjects.ApplicationSectionBases;

import org.openqa.selenium.By;
import pageobjects.BaseSections.ApplicantInfoBase;
import pageobjects.GWRadioBinaryYesNo;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;

public abstract class ApplicationApplicantInfoBase<A extends ApplicantInfoBase> extends ApplicantInfoBase
{
	ApplicationApplicantInfoBaseBy by = new ApplicationApplicantInfoBaseBy();
	public GWRadioBinaryYesNo mailingAddressSameAsLocationAddress = new GWRadioBinaryYesNo(By.cssSelector("[label='Mailing address same as location address?']"));

	public class ApplicationApplicantInfoBaseBy
	{
		public final By mailingAddress1 = By.cssSelector("[ng-show*='mailingAddressMatches'] [label='Mailing Address 1'] input"),
						mailingAddress2 = By.cssSelector("[ng-show*='mailingAddressMatches'] [label='Mailing Address 2'] input"),
						mailingAddress3 = By.cssSelector("[ng-show*='mailingAddressMatches'] [label='Mailing Address 3'] input"),
						mailingAddressCity = By.cssSelector("[ng-show*='mailingAddressMatches'] [label='City'] input"),
						mailingAddressZip = By.cssSelector("[ng-show*='mailingAddressMatches'] [label='ZIP Code'] input"),
						mailingAddressState = By.cssSelector("[ng-show*='mailingAddressMatches'] [label='State'] select"),
						addressType = By.cssSelector("[label='Address Type'] select");
	}

	public A setAppApplicantInfoFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
	{
		Boolean mailingAddressSameAsLocation = hashMap.get("PolicyApplication&LocationInfoMailingaddressthesameaslocationaddress").equals("Yes");

		showSectionBase();

		setDateOfBirth("04/18/1990");
		setSSN("123-12-1234");
		setOccupation("Welder");
		mailingAddressSameAsLocationAddress.clickYesOrNo(mailingAddressSameAsLocation);
		if(!mailingAddressSameAsLocation)
		{
			setMailingAddress1(hashMap.get("PolicyApplication&LocationInfoMailingaddress1"));
			setMailingAddressCity(hashMap.get("PolicyApplication&LocationInfoCity"));
			setZip(hashMap.get("PolicyApplication&LocationInfoZipCode"));
			selectState(hashMap.get("PolicyApplication&LocationInfoState"));
			selectAddressType(hashMap.get("PolicyApplication&LocationInfoAddressType"));
		}

		String[] phoneInfo;

		if(!hashMap.get("PolicyApplication&LocationInfoHomePhone").contains("blank"))
		{
			phoneInfo = hashMap.get("PolicyApplication&LocationInfoHomePhone").split(" - ");
			setHomePhone(phoneInfo[0]);
			if(phoneInfo.length > 1 && phoneInfo[1].contains("primary"))
				setHomePhoneAsPrimary();
		}
		if(!hashMap.get("PolicyApplication&LocationInfoWorkPhone").contains("blank"))
		{
			phoneInfo = hashMap.get("PolicyApplication&LocationInfoWorkPhone").split(" - ");
			setWorkPhone(phoneInfo[0]);
			if(phoneInfo.length > 1 && phoneInfo[1].contains("primary"))
				setWorkPhoneAsPrimary();
		}
		if(!hashMap.get("PolicyApplication&LocationInfoCell").contains("blank"))
		{
			phoneInfo = hashMap.get("PolicyApplication&LocationInfoCell").split(" - ");
			setCellPhone(phoneInfo[0]);
			if(phoneInfo.length > 1 && phoneInfo[1].contains("primary"))
				setCellPhoneAsPrimary();
		}
		return (A)this;
	}

	public A setDateOfBirth(String dateOfBirth)
	{
		super.setDateOfBirthBase(dateOfBirth);
		return (A)this;
	}

	public A setSSN(String SSN)
	{
		super.setSSNBase(SSN);
		return (A)this;
	}

	public A setHomePhone(String homePhone)
	{
		super.setHomePhoneBase(homePhone);
		return (A)this;
	}

	public A setHomePhoneAsPrimary()
	{
		super.setHomePhoneAsPrimaryBase();
		return (A)this;
	}

	public A setWorkPhone(String workPhone)
	{
		super.setWorkPhoneBase(workPhone);
		return (A)this;
	}

	public A setWorkPhoneAsPrimary()
	{
		super.setWorkPhoneAsPrimaryBase();
		return (A)this;
	}

	public A setCellPhone(String cellPhone)
	{
		super.setCellPhoneBase(cellPhone);
		return (A)this;
	}

	public A setCellPhoneAsPrimary()
	{
		super.setCellPhoneAsPrimaryBase();
		return (A)this;
	}

	public A setOccupation(String occupation)
	{
		setOccupationBase(occupation);
		return (A)this;
	}

	public A setMailingAddress1(String addressLine1)
	{
		gh.setText(by.mailingAddress1, addressLine1);
		return (A)this;
	}

	public A setMailingAddress2(String addressLine2)
	{
		gh.setText(by.mailingAddress2, addressLine2);
		return (A)this;
	}

	public A setMailingAddress3(String addressLine3)
	{
		gh.setText(by.mailingAddress3, addressLine3);
		return (A)this;
	}

	public A setMailingAddressCity(String city)
	{
		gh.setText(by.mailingAddressCity, city);
		return (A)this;
	}

	public A setZip(String zip)
	{
		gh.setText(by.mailingAddressZip, zip);
		return (A)this;
	}

	public A selectState(String state)
	{
		ghs.selectOption(by.mailingAddressState, state);
		return (A)this;
	}

	public A selectAddressType(String addressType)
	{
		ghs.selectOption(by.addressType, addressType);
		return (A)this;
	}
}