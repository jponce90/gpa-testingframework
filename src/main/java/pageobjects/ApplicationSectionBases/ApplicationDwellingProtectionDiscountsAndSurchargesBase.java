package pageobjects.ApplicationSectionBases;

import org.openqa.selenium.By;
import pageobjects.BaseSections.DwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class ApplicationDwellingProtectionDiscountsAndSurchargesBase<D extends DwellingProtectionDiscountsAndSurchargesBase> extends DwellingProtectionDiscountsAndSurchargesBase
{
	public final GWRadioBinaryYesNo lockedFence = new GWRadioBinaryYesNo(By.cssSelector("[label='Is there a locked fence?'"));
	protected final GWRadioBinaryYesNo burglarBarsBase = new GWRadioBinaryYesNo(By.cssSelector("[label='Are there any burglar bars on the windows/doors?']")),
										safetyLatches = new GWRadioBinaryYesNo(By.cssSelector("[label='Are there safety latches present?']")),
										fireExtinguishers = new GWRadioBinaryYesNo(By.cssSelector("[label='One or more fire extinguishers in the home?']")),
										deadbolts = new GWRadioBinaryYesNo(By.cssSelector("[label='Deadbolts")),
										residenceVisibleToNeighbours = new GWRadioBinaryYesNo(By.cssSelector("[label='Residence visible to neighbours?']"));

	public void setAppDwellingProtectionDiscountsAndSurchargesFromHashMapBase(LinkedHashMap hashMap)
	{
		showSectionDwellingProtectionDiscountsAndSurcharges();

		lockedFence.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesIstherealockedfence").equals("Yes"));
	}
}