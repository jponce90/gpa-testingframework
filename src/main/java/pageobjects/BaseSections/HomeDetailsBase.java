package pageobjects.BaseSections;

import org.openqa.selenium.By;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.GWRadioBinary;

import java.util.LinkedHashMap;

public abstract class HomeDetailsBase extends SectionBase
{
	protected final HomeDetailsBy by = new HomeDetailsBy();

	public HomeDetailsBase()
	{
		super();
	}

	protected void showSectionHomeDetails()
	{
		showSectionBase(by.sectionHeader, by.yearBuiltModel);
	}

	public final class HomeDetailsBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Home Details')]"),
						dwellingLimit = By.cssSelector("input[ng-model*='dwellingLimit']"),
						personalProperty = By.cssSelector("[label='Personal Property'] input"),
						constructionType = By.cssSelector("[label='Construction Type'] select"),
						constructionTypeReadOnly = By.cssSelector("[label='Construction Type'] .ng-scope span"),
						protectionClass = By.cssSelector("[label='Protection Class'] select"),
						bceg = By.cssSelector("[label='BCEG'] select"),
						territoryCode = By.cssSelector("[label='Territory Code'] input"),
						residenceType = By.cssSelector("[label='Residence Type'] select"),
						usageType = By.cssSelector("[label='Usage Type?'] select"),
						howIsDwellingOccupied = By.cssSelector("[label='How is dwelling occupied?'] select"),
						yearBuilt = By.cssSelector("[label*='Year Built'] input"),
						yearBuiltReadOnly = By.cssSelector("[label*='Year Built'] .ng-scope span"),
						yearBuiltModel = By.cssSelector("gw-pl-input-ctrl[model*='yourHomeView.yearBuilt']");
	}

	protected void setHomeDetailsFromHashMapBase(LinkedHashMap<String, String> hashMap)
	{
		String protectionClass = hashMap.get("HomeDetailsProtectionClass");

		showSectionHomeDetails();

		setYearBuiltBase(hashMap.get("HomeDetailsYearbuilt"));
		if(!protectionClass.contains("system prefills"))
			selectProtectionClassBase(protectionClass);
		if(hashMap.containsKey("HomeDetailsBCEG") && !hashMap.get("HomeDetailsBCEG").contains("system prefills"))
			selectBCEGBase(hashMap.get("HomeDetailsBCEG"));
		if(hashMap.containsKey("HomeDetailsTerritoryCode") && !hashMap.get("HomeDetailsTerritoryCode").contains("system prefills"))
			setTerritoryCodeBase(hashMap.get("HomeDetailsTerritoryCode"));
	}

	protected void setDwellingLimitBase(String dwellingLimit)
	{
		gh.setText(by.dwellingLimit, dwellingLimit);
		gh.tab();
		gh.wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("class*='backdrop'")));
	}

	protected String getDwellingLimitBase()
	{
		return gh.getValue(by.dwellingLimit);
	}

	protected void setPersonalPropertyBase(String personalProperty)
	{
		gh.setText(by.personalProperty, personalProperty);
		gh.tab();
		gh.wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("class*='backdrop'")));
	}

	protected String getPersonalProperty()
	{
		return gh.getValue(by.personalProperty);
	}

	public String getYearBuilt()
	{
		return gh.getValue(by.yearBuilt);
	}

	protected void setYearBuiltBase(String yearBuilt)
	{
		gh.setText(by.yearBuilt, yearBuilt);
		gh.tab();
	}

	public String getConstructionType()
	{
		return ghs.getSelectedOption(by.constructionType);
	}

	protected void selectConstructionTypeBase(String constructionType)
	{
		ghs.selectOption(by.constructionType, constructionType);
	}

	protected void selectProtectionClassBase(String protectionClass)
	{
		ghs.selectOption(by.protectionClass, protectionClass);
	}

	protected void selectBCEGBase(String bceg)
	{
		ghs.selectOption(by.bceg, bceg);
	}

	protected void setTerritoryCodeBase(String territoryCode)
	{
		gh.setText(by.territoryCode, territoryCode);
	}

	public String getResidenceType()
	{
		return ghs.getSelectedOption(by.residenceType);
	}

	protected void selectResidenceTypeBase(String residenceType)
	{
		ghs.selectOption(by.residenceType, residenceType);
	}

	public String getUsageType()
	{
		return ghs.getSelectedOption(by.usageType);
	}

	protected void selectUsageTypeBase(String usageType)
	{
		ghs.selectOption(by.usageType, usageType);
	}

	public String getHowIsDwellingOccupied()
	{
		return ghs.getSelectedOption(by.howIsDwellingOccupied);
	}

	protected void selectHowIsDwellingOccupiedBase(String howIsDwellingOccupied)
	{
		ghs.selectOption(by.howIsDwellingOccupied, howIsDwellingOccupied);
	}

	protected void setTerritoryCode(String territoryCode)
	{
		gh.setText(by.territoryCode, territoryCode);
	}

	public final class DistanceToFireHydrant extends GWRadioBinary
	{
		public DistanceToFireHydrant()
		{
			super(By.cssSelector("[label='Distance to Fire Hydrant'"));
		}

		public DistanceToFireHydrant setWithin1000ft()
		{
			setWithin1000ftBase();
			return this;
		}

		public DistanceToFireHydrant setOver1000ft()
		{
			setOver1000ftBase();
			return this;
		}

		public String getDistanceToFireHydrant()
		{
			return gh.getAttribute(By.cssSelector("[label=\"Distance to Fire Hydrant\"] [aria-checked='true']"), "id").endsWith("Left") ? "Within 1000ft" : "Over 1000ft";
		}

		public String getDistanceToFireHydrantReadOnly()
		{
			return gh.getText(new ByChained(by, By.cssSelector(".gw-controls .ng-scope span")));
		}
	}

	public class DistanceToCoastRadio extends GWRadioBinary
	{
		public DistanceToCoastRadio()
		{
			super(By.cssSelector("[label='Distance to coast'"));
		}

		public DistanceToCoastRadio setWithin1000ft()
		{
			setWithin1000ftBase();
			return this;
		}

		public DistanceToCoastRadio setOver1000ft()
		{
			setOver1000ftBase();
			return this;
		}

		public String getDistanceToCoast()
		{
			return gh.getAttribute(By.cssSelector("[label=\"Distance to coast\"] [aria-checked='true']"), "id").endsWith("Left") ? "Within 1000ft" : "Over 1000ft";
		}
	}
}