package pageobjects.BaseSections;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public abstract class ApplicantInfoBase extends SectionBase
{
	public final GPAHelper gh;
	public final GPAHelper.SelectHelper ghs;
	public final WebDriver driver;
	public ApplicantInfoBaseBy sectionBaseBy = new ApplicantInfoBaseBy();

	public ApplicantInfoBase()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		ghs = gh.new SelectHelper();
	}

	//TODO find item to wait for
	protected void showSectionBase()
	{
		showSectionBase(sectionBaseBy.sectionHeader, sectionBaseBy.dateOfBirth);
	}

	public final class ApplicantInfoBaseBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Policy, Applicant & Location Info')]"),
						dateOfBirth = By.id("DateOfBirth"),
						SSN = By.cssSelector("[label='SSN'] input"),
						occupation = By.cssSelector("[label='Occupation'] input"),
						homePhone = By.cssSelector("input[ng-model*='homeNumber']"),
						homePhonePrimary = By.cssSelector("div[model='phone.homeNumber'] .gw-controls label"),
						workPhone = By.cssSelector("input[ng-model*='workNumber']"),
						workPhonePrimary = By.cssSelector("div[model='phone.workNumber'] .gw-controls label"),
						cellPhone = By.cssSelector("input[ng-model*='cellNumber']"),
						cellPhonePrimary = By.cssSelector("div[model='phone.cellNumber'] .gw-controls label");
	}

	protected void setDateOfBirthBase(String dateOfBirth)
	{
		By datePicker = By.className("gw-open");
		gh.setText(sectionBaseBy.dateOfBirth, dateOfBirth);
		if(gh.isDisplayed(sectionBaseBy.dateOfBirth))
		{
			gh.clickElement(sectionBaseBy.dateOfBirth);
			gh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(datePicker));
		}
	}

	protected void setSSNBase(String SSN)
	{
		gh.setText(sectionBaseBy.SSN, SSN);
	}

	protected void setOccupationBase(String occupation)
	{
		gh.setText(sectionBaseBy.occupation, occupation);
	}

	protected void setHomePhoneBase(String homePhone)
	{
		gh.setText(sectionBaseBy.homePhone, homePhone);
	}

	protected void setHomePhoneAsPrimaryBase()
	{
		gh.clickElement(sectionBaseBy.homePhonePrimary);
	}

	protected void setWorkPhoneBase(String workPhone)
	{
		gh.setText(sectionBaseBy.workPhone, workPhone);
	}

	protected void setWorkPhoneAsPrimaryBase()
	{
		gh.clickElement(sectionBaseBy.workPhonePrimary);
	}

	protected void setCellPhoneBase(String cellPhone)
	{
		gh.setText(sectionBaseBy.cellPhone, cellPhone);
	}

	protected void setCellPhoneAsPrimaryBase()
	{
		gh.clickElement(sectionBaseBy.cellPhonePrimary);
	}
}