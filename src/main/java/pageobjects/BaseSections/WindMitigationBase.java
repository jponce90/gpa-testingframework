package pageobjects.BaseSections;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;
public abstract class WindMitigationBase extends SectionBase
{
	protected WebDriver driver;
	protected GPAHelper gh;
	protected GPAHelper.SelectHelper ghs;
	protected final WindMitigationBy by = new WindMitigationBy();
	protected final GWRadioBinaryYesNo secondaryWaterResistanceBase = new GWRadioBinaryYesNo(By.cssSelector("[label='Secondary Water Resistance'")),
										roofCoverBuildingCodeCompliantBase = new GWRadioBinaryYesNo(By.cssSelector("[label='Is the Roof Cover Construction Building Code Compliant?']")),
										roofWallBuildingCodeCompliantBase = new GWRadioBinaryYesNo(By.cssSelector("[label='Is the roof wall connection building code compliant?']")),
										roofDeckBuildingCodeCompliantBase = new GWRadioBinaryYesNo(By.cssSelector("[label='Is the roof deck attachment building code compliant?']"));

	public WindMitigationBase()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper();
		ghs = gh.new SelectHelper();
	}

	protected void showSectionWindMitigation()
	{
		showSectionBase(by.sectionHeader, by.roofYear);
	}

	public final class WindMitigationBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Wind Mitigation Info')]"),
						roofYear = By.cssSelector("[model*='view.roofYear_fli'] input"),
						roofType = By.cssSelector("[label='Roof Type'] select"),
						roofDeck = By.cssSelector("[label='Roof Deck'] select"),
						roofShape = By.cssSelector("[label='Roof Shape'] select"),
						discountType = By.cssSelector("[label='Discount Type'] select"),
						fortifiedHome = By.cssSelector("[label='Fortified Home'] select"),
						roofCover = By.cssSelector("[label='Roof Cover'] select"),
						roofWallConnection = By.cssSelector("[label='Roof Wall Connection'] select"),
						openingProtection = By.cssSelector("[label='Opening Protection'] select");
	}

	protected void setWindMitigationFromHashMapBase(LinkedHashMap<String, String> hashMap)
	{
		showSectionWindMitigation();
	}

	protected void setRoofYearBase(String roofYear)
	{
		gh.setText(by.roofYear, roofYear);
		gh.tab();
	}

	protected String getRoofYear()
	{
		return gh.getValue(by.roofYear);
	}

	protected void selectRoofTypeBase(String roofType)
	{
		ghs.selectOption(by.roofType, roofType);
	}

	protected String getRoofType()
	{
		return ghs.getSelectedOption(by.roofType);
	}

	protected void selectRoofShapeBase(String roofShape)
	{
		ghs.selectOption(by.roofShape, roofShape);
	}

	protected String getRoofShape()
	{
		return ghs.getSelectedOption(by.roofShape);
	}

	protected void selectRoofDeckBase(String roofDeck)
	{
		ghs.selectOption(by.roofDeck, roofDeck);
	}

	protected String getRoofDeck()
	{
		return ghs.getSelectedOption(by.roofDeck);
	}

	protected void selectRoofCoverBase(String roofCover)
	{
		ghs.selectOption(by.roofCover, roofCover);
	}

	protected String getRoofCover()
	{
		return ghs.getSelectedOption(by.roofCover);
	}

	protected void selectRoofWallConnectionBase(String roofWallConnection)
	{
		ghs.selectOption(by.roofWallConnection, roofWallConnection);
	}

	protected String getRoofWallConnection()
	{
		return ghs.getSelectedOption(by.roofWallConnection);
	}

	protected void selectOpeningProtectionBase(String openingProtection)
	{
		ghs.selectOption(by.openingProtection, openingProtection);
	}

	protected String getOpeningProtection()
	{
		return ghs.getSelectedOption(by.openingProtection);
	}

	protected void selectDiscountTypeBase(String discountType)
	{
		ghs.selectOption(by.discountType, discountType);
	}

	protected String getDiscountType()
	{
		return ghs.getSelectedOption(by.discountType);
	}

	protected void selectFortifiedHomeBase(String discountType)
	{
		ghs.selectOption(by.fortifiedHome, discountType);
	}

	protected String getFortifiedHome()
	{
		return ghs.getSelectedOption(by.fortifiedHome);
	}
}