package pageobjects.BaseSections;

import org.openqa.selenium.By;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class DwellingProtectionDiscountsAndSurchargesBase extends SectionBase
{
	public DwellingProtectionDiscountsAndSurchargesBaseBy by = new DwellingProtectionDiscountsAndSurchargesBaseBy();
	protected GWRadioBinaryYesNo seniorCitizenDiscountBase = new GWRadioBinaryYesNo(By.cssSelector("[label='Senior Citizen Discount']"));
	public GWRadioBinaryYesNo burglarAlarm = new GWRadioBinaryYesNo(By.cssSelector("[label='Burglar Alarm']"));
	public GWRadioBinaryYesNo guardedCommunity = new GWRadioBinaryYesNo(By.cssSelector("[label='Guarded Community']"));
	public GWRadioBinaryYesNo gatedCommunity = new GWRadioBinaryYesNo(By.cssSelector("[label='Gated Community']"));
	public GWRadioBinaryYesNo fireAlarm = new GWRadioBinaryYesNo(By.cssSelector("[label='Fire Alarm']"));
	public GWRadioBinaryYesNo smokeAlarms = new GWRadioBinaryYesNo(By.cssSelector("[label='Smoke Alarms']"));
	public GWRadioBinaryYesNo fireSprinklers = new GWRadioBinaryYesNo(By.cssSelector("[label='Fire Sprinklers']"));

	public DwellingProtectionDiscountsAndSurchargesBase()
	{
		super();
	}

	private final class DwellingProtectionDiscountsAndSurchargesBaseBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Dwelling Protection, Discounts and Surcharges')]"),
						burglarAlarmModel = By.cssSelector("[model*='burglarAlarm']"),
						burglarAlarmType = By.cssSelector("[model*='burglarAlarmType'] select"),
						fireSprinklersType = By.cssSelector("[model*='sprinklerSystemType'] select"),
						fireAlarmType = By.cssSelector("[model*='fireAlarmType'] select");
	}

	protected void setDwellingProtectionDiscountsAndSurchargesFromHashMapBase(LinkedHashMap<String, String> hashMap)
	{
		showSectionDwellingProtectionDiscountsAndSurcharges();

		guardedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGuardedCommunity").equals("Yes"));
		gatedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGatedCommunity").equals("Yes"));
	}

	protected void showSectionDwellingProtectionDiscountsAndSurcharges()
	{
		showSectionBase(by.sectionHeader, by.burglarAlarmModel);
	}

	public void selectBurglarAlarmType(String burglarAlarmType)
	{
		ghs.selectOption(by.burglarAlarmType, burglarAlarmType);
	}

	public String getBurglarAlarmType()
	{
		return ghs.getSelectedOption(by.burglarAlarmType);
	}

	public void selectFireAlarmType(String fireAlarmType)
	{
		ghs.selectOption(by.fireAlarmType, fireAlarmType);
	}

	public String getFireAlarmType()
	{
		return ghs.getSelectedOption(by.fireAlarmType);
	}

	public void selectFireSprinklersType(String fireSprinklersType)
	{
		ghs.selectOption(by.fireSprinklersType, fireSprinklersType);
	}

	public String getFireSprinklersType()
	{
		return ghs.getSelectedOption(by.fireSprinklersType);
	}
}