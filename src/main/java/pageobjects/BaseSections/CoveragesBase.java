package pageobjects.BaseSections;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public abstract class CoveragesBase<H> extends SectionBase
{
	public final GPAHelper gh;
	public final GPAHelper.SelectHelper ghs;
	public final WebDriver driver;
	public final GPAHelper.ModalHelper mh;
	public CoveragesBaseBy by = new CoveragesBaseBy();

	public CoveragesBase()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		ghs = gh.new SelectHelper();
		mh = gh.new ModalHelper();
	}

	public final class CoveragesBaseBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Coverages')]"),
						validationModalHeader = By.xpath("//h3[contains(.,'Validation Rules')]"),
						modalClose = By.cssSelector(".gw-modal-footer button[ng-click*='dismiss']"),
						quoteTabs = By.className("gw-quote-tabs-main"),
						lessCoverageLabel = By.xpath("//div[contains(@class,'gw-quote-tabs-main')]//ul[contains(@class,'gw-nav')]//li[contains(.,'Less Coverage')]"),
						mostPopularLabel = By.xpath("//div[contains(@class,'gw-quote-tabs-main')]//ul[contains(@class,'gw-nav')]//li[contains(.,'Most Popular')]"),
						moreCoverageLabel = By.xpath("//div[contains(@class,'gw-quote-tabs-main')]//ul[contains(@class,'gw-nav')]//li[contains(.,'More Coverage')]"),
						continueToApp = By.xpath("//div[contains(@class,'gw-quote-tabs-main')]//div[contains(@class,'gw-tab-content')]//div[contains(@class,'gw-active')]//button[contains(.,'Continue to application')]");
	}

	public void showSection()
	{
		showSectionBase(by.sectionHeader, by.quoteTabs, 90);
	}

	public void clickShowSection()
	{
		gh.clickElement(by.sectionHeader);
	}

	public void closeValidationMessage()
	{
		gh.wait(30).until(ExpectedConditions.visibilityOfElementLocated(by.validationModalHeader));
		driver.findElement(by.modalClose).click();
	}

	public void showSectionIgnoreValidationWarning()
	{
		gh.wait(5).until(ExpectedConditions.visibilityOfElementLocated(by.sectionHeader));
		gh.clickElement(by.sectionHeader);
		gh.wait(90).until((WebDriver driver) -> {
			if(gh.isDisplayed(by.validationModalHeader))
				driver.findElement(by.modalClose).click();
			return gh.isDisplayed(by.quoteTabs);
		});
	}
	public void clickContinueToApplication()
	{
		gh.wait(5).until(ExpectedConditions.elementToBeClickable(by.continueToApp));
		gh.clickElement(by.continueToApp);
	}

	protected void clickMoreCoverageBase()
	{
		gh.wait(5).until(ExpectedConditions.elementToBeClickable(by.moreCoverageLabel));
//		gh.clickElement(by.moreCoverageLabel);
	}

	protected void clickMostPopularBase()
	{
		gh.wait(5).until(ExpectedConditions.elementToBeClickable(by.mostPopularLabel));
		gh.clickElement(by.mostPopularLabel);
	}

	protected void clickLessCoverageBase()
	{
		gh.wait(5).until(ExpectedConditions.elementToBeClickable(by.lessCoverageLabel));
		gh.clickElement(by.lessCoverageLabel);
	}
}