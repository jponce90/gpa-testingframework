package pageobjects.BaseSections;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public abstract class QualificationBase extends SectionBase
{
	public QualificationBaseBy by = new QualificationBaseBy();

	public QualificationBase()
	{
		super();
	}

	//TODO add a wait for a field to appear
	public void showSectionQualification()
	{
		showSectionBase(by.sectionHeader, by.wiringQuestion);
	}

	public final class QualificationBaseBy
	{
		public final By sectionHeader = By.xpath("//h3[contains(.,'Qualification')]"),
						wiringQuestion = By.cssSelector(".gw-prequal-questions div[ng-repeat*='questionSets']>.ng-scope div[label='Is there any knob & tube or single-strand aluminum wiringQuestion in the home?'] .gw-radios-binary");
	}
}