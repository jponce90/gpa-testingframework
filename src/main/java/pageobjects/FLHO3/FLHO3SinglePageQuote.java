package pageobjects.FLHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.WebDriver;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.QuoteSectionBases.QuoteDwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.QuoteSectionBases.QuoteHomeDetailsBase;
import pageobjects.QuoteSectionBases.QuoteWindMitigationBase;
import pageobjects.SinglePageQuoteBase;

import java.util.LinkedHashMap;

public class FLHO3SinglePageQuote extends SinglePageQuoteBase
{
	private WebDriver driver;
	private GPAHelper gh;
	public FLHO3QuoteHomeDetails homeDetails;
	public FLHO3QuoteWindMitigation windMitigation;
	public FLHO3QuoteDwellingProtectionDiscountsAndSurcharges dwellingProtectionDiscountsAndSurcharges;
	public FLHO3QuoteCoverages coverages;

	public FLHO3SinglePageQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		homeDetails = new FLHO3QuoteHomeDetails();
		windMitigation = new FLHO3QuoteWindMitigation();
		coverages = new FLHO3QuoteCoverages();
		dwellingProtectionDiscountsAndSurcharges = new FLHO3QuoteDwellingProtectionDiscountsAndSurcharges();
	}

	/**The following classes hold methods that affect fields that appear
	 * in both the quote as well as applications for FLHO3 exclusively*/
	public class FLHO3QuoteHomeDetails extends QuoteHomeDetailsBase<FLHO3QuoteHomeDetails>
	{
		public FLHO3QuoteHomeDetails showSection()
		{
			showSectionHomeDetails();
			return this;
		}

		public FLHO3QuoteHomeDetails setHomeDetailsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection();

			setHomeDetailsFromHashMapBase(hashMap);
			setDwellingLimit(hashMap.get("HomeDetailsDwellingLimit"))
			.selectConstructionType(hashMap.get("HomeDetailsConstructionType"));
			if(hashMap.get("HomeDetailsDistancetoFireHydrant").equals("Within 1000 ft"))
				distanceToFireHydrant.setWithin1000ft();
			else
				distanceToFireHydrant.setOver1000ft();
			selectResidenceType(hashMap.get("HomeDetailsResidenceType"))
			.selectUsageType(hashMap.get("HomeDetailsUsageType"))
			.selectHowIsDwellingOccupied(hashMap.get("HomeDetailsHowisdwellingoccupied"));
			return this;
		}

		public FLHO3QuoteHomeDetails setDwellingLimit(String dwellingLimit)
		{
			return super.setDwellingLimit(dwellingLimit);
		}

		public String getDwellingLimit()
		{
			return super.getDwellingLimit();
		}

		public FLHO3QuoteHomeDetails selectProtectionClass(String protectionClass)
		{
			selectProtectionClassBase(protectionClass);
			return this;
		}

		public FLHO3QuoteHomeDetails selectBCEG(String bceg)
		{
			selectBCEGBase(bceg);
			return this;
		}

		public FLHO3QuoteHomeDetails selectTerritoryCode(String territoryCode)
		{
			setTerritoryCodeBase(territoryCode);
			return this;
		}
	}

	public class FLHO3QuoteWindMitigation extends QuoteWindMitigationBase<FLHO3QuoteWindMitigation>
	{
		public FLHO3QuoteWindMitigation showSection()
		{
			super.showSectionWindMitigation();
			return this;
		}

		public FLHO3QuoteWindMitigation setWindMitigationFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection()
			.setRoofYear(hashMap.get("WindMitigationRoofYear"))
			.selectRoofType(hashMap.get("WindMitigationRoofType"))
			.selectRoofShape(hashMap.get("WindMitigationRoofShape"))
			.secondaryWaterResistanceBase.clickYesOrNo(hashMap.get("WindMitigationSecondaryWaterResistance").equals("Yes"));
			if(!hashMap.get("WindMitigationOpeningProtection").contains("not"))
				selectOpeningProtection(hashMap.get("WindMitigationOpeningProtection"));

			int yearBuilt = Integer.parseInt(hashMap.get("HomeDetailsYearbuilt"));
			if(yearBuilt < 2002)
				selectRoofCover(hashMap.get("WindMitigationRoofCover"));
			//TODO IF ROOF DECK ATTACHMENT
			if(yearBuilt < 2002)
				selectRoofWallConnection(hashMap.get("WindMitigationRoofWallConnection"));
			if(yearBuilt> 2001)
				selectRoofDeck(hashMap.get("WindMitigationRoofDeck"));
			//TODO PREFILLS

			return this;
		}
	}

	public class FLHO3QuoteDwellingProtectionDiscountsAndSurcharges extends QuoteDwellingProtectionDiscountsAndSurchargesBase<FLHO3QuoteDwellingProtectionDiscountsAndSurcharges>
	{
		public FLHO3QuoteDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public FLHO3QuoteDwellingProtectionDiscountsAndSurcharges setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean fireAlarm = hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";")[0].equals("Yes"),
					fireSprinklers = hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";")[0].equals("Yes"),
					burglarAlarm = hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";")[0].equals("Yes");
			showSection()
			.burglarAlarm.clickYesOrNo(burglarAlarm);
			if(burglarAlarm)
				selectBurglarAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";[ ]{0,1}")[1]);
			this.guardedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGuardedCommunity").equals("Yes"));
			this.gatedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGatedCommunity").equals("Yes"));
			this.fireAlarm.clickYesOrNo(fireAlarm);
			if(fireAlarm)
				selectFireAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";[ ]{0,1}")[1]);
			this.smokeAlarms.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSmokeAlarms").equals("Yes"));
			this.fireSprinklers.clickYesOrNo(fireSprinklers);
			if(fireSprinklers)
				selectFireSprinklersType("Full");
				//TODO input just says 'enter Sprinkler type'
//				selectFireSprinklersType(hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";[ ]{0,1}")[1]);
			return this;
		}
	}

	public class FLHO3QuoteCoverages extends CoveragesBase<FLHO3QuoteCoverages>
	{
		public FLHO3SinglePageApplication continueToApp()
		{
			super.clickContinueToApplication();
			return new FLHO3SinglePageApplication();
		}

		public FLHO3QuoteCoverages clickMoreCoverage()
		{
			super.clickMoreCoverageBase();
			return this;
		}

		public FLHO3QuoteCoverages clickMostPopular()
		{
			super.clickMostPopularBase();
			return this;
		}

		public FLHO3QuoteCoverages clickLessCoverage()
		{
			super.clickLessCoverageBase();
			return this;
		}
	}

}