package pageobjects.FLHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.PossibleAccountMatches;
import pageobjects.SideBar;

public class FLHO3
{
	public FLHO3SinglePageQuote quote;
	public FLHO3SinglePageApplication app;
	public SideBar sideBar;
	private PossibleAccountMatches possibleAccountMatches;
	public FLHO3()
	{
		quote = new FLHO3SinglePageQuote();
		app = new FLHO3SinglePageApplication();
		possibleAccountMatches = new PossibleAccountMatches();
		sideBar = new SideBar();
	}

	public FLHO3SinglePageApplication continueAsNewCustomer()
	{
		possibleAccountMatches.clickContinueAsNewCustomer();
		new GPAHelper(LocalDriverManager.getDriver()).wait(45).until(ExpectedConditions.elementToBeClickable(app.applicantInfo.sectionBaseBy.sectionHeader));
		return app;
	}
}