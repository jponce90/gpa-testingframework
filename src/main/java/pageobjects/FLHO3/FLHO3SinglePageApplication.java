package pageobjects.FLHO3;

import pageobjects.ApplicationSectionBases.*;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class FLHO3SinglePageApplication extends FLHO3SinglePageQuote
{
	public FLHO3AppApplicantInfo applicantInfo;
	public FLHO3AppHomeDetails homeDetails;
	public FLHO3AppWindMitigation windMitigation;
	public FLHO3AppCoverages coverages;
	public FLHO3AppQualification qualification;
	public FLHO3AppDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;
	public LossHistoryBase lossHistory;
	public BindPolicyBase bindPolicy;

	public FLHO3SinglePageApplication()
	{
		applicantInfo = new FLHO3AppApplicantInfo();
		homeDetails = new FLHO3AppHomeDetails();
		windMitigation = new FLHO3AppWindMitigation();
		coverages = new FLHO3AppCoverages();
		qualification = new FLHO3AppQualification();
		dwellingProtectionAndSurcharges = new FLHO3AppDwellingProtectionDiscountsAndSurcharges();
		lossHistory = new LossHistoryBase();
		bindPolicy = new BindPolicyBase();
	}

	public class FLHO3AppApplicantInfo extends ApplicationApplicantInfoBase<FLHO3AppApplicantInfo>
	{
		public FLHO3AppApplicantInfo showSection()
		{
			showSectionBase();
			return this;
		}
	}

	public class FLHO3AppQualification extends ApplicationQualificationBase<FLHO3AppQualification>
	{
		public FLHO3AppQualification showSection()
		{
			showSectionQualification();
			return this;
		}
	}

	public class FLHO3AppHomeDetails extends ApplicationHomeDetailsBase<FLHO3AppHomeDetails>
	{
		public FLHO3AppHomeDetails showSection()
		{
			super.showSectionHomeDetails();
			return this;
		}

		public FLHO3AppHomeDetails setAppHomeDetailFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			/**
			 * Show Home Details Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean inTheWindpool = hashMap.get("HomeDetailsInthewindpool").equals("Yes"),
					swimmingPool = hashMap.get("HomeDetailsIsthereaswimmingpool").equals("Yes"),
					secondaryHeating = hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"),
					animal = hashMap.get("HomeDetailsAnyanimalorexoticpets").equals("Yes");
			String wiringType = hashMap.get("HomeDetailsWiring"),
					wiringDescription = hashMap.get("HomeDetailsWiringtypedescription");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			selectValuationTypeBase(hashMap.get("HomeDetailsValuationType"))
			.setReplacementCost(hashMap.get("HomeDetailsEstimatedReplacementCost"))
			.setDistanceToFireStation(hashMap.get("HomeDetailsDistancetoFireStation"))
			.selectLocationType(hashMap.get("HomeDetailsLocationType"))
			.inTheWindPool.clickYesOrNo(inTheWindpool);
			if(hashMap.get("HomeDetailsDistancetoCoast").equals("Within 1000 ft"))
				distanceToCoast.setWithin1000ft();
			else
				distanceToCoast.setOver1000ft();
			//TODO set Purchase Date
			//TODO set Purchase Price
			//TODO market value
			occupiedDaily.clickYesOrNo(hashMap.get("HomeDetailsOccupiedDaily").equals("Yes"));
			this.swimmingPool.clickYesOrNo(swimmingPool);
			if(swimmingPool)
			{
				selectPoolLocation(hashMap.get("HomeDetailsPoolLocation"))
				.poolFenced.clickYesOrNo(hashMap.get("HomeDetailsIsthepoolFenced").equals("Yes"));
				divingBoard.clickYesOrNo(hashMap.get("HomeDetailsIsthereadivingboard").equals("Yes"));
				poolSlide.clickYesOrNo(hashMap.get("HomeDetailsIsthereapoolslide").equals("Yes"));
			}
			trampoline.clickYesOrNo(hashMap.get("HomeDetailsIsthereatrampoline").equals("Yes"));
			animals.clickYesOrNo(animal);
			if(animal)
			{
				//TODO fill out animal information
			}

			golfCarts.clickYesOrNo(hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"));
			recreationVehicles.clickYesOrNo(hashMap.get("HomeDetailsAnyownedrecreationvehicles").equals("Yes"));
			selectHousekeepingCondition(hashMap.get("HomeDetailsHousekeepingcondition"));
			selectNumberOfUnits(hashMap.get("HomeDetailsNumberofUnits"));
			selectUnitInFireWalls(hashMap.get("HomeDetailsUnitsinFireWalls"));
			setNumberOfStories(hashMap.get("HomeDetailsNumberofStories"));
			setSquareFootage(hashMap.get("HomeDetailsSquareFootage"));
			selectFoundationType(hashMap.get("HomeDetailsFoundationType"));
			selectPrimaryHeating(hashMap.get("HomeDetailsPrimaryHeating"));
			secondaryHeatingSystem.clickYesOrNo(secondaryHeating);
			selectPlumbing(hashMap.get("HomeDetailsPlumbing"))
			.setPlumbingYear(hashMap.get("HomeDetailsPlumbingyear"))
			.setWaterHeaterYear(hashMap.get("HomeDetailsWaterHeaterYear"))
			.selectWiring(wiringType);

			if(wiringType.equals("Other") && !wiringDescription.contains("blank"))
				setWiringDescription(wiringDescription);

			selectElectricalSystem(hashMap.get("HomeDetailsElectricalSystem"))
			.selectConditionOfRoof(hashMap.get("HomeDetailsConditionofRoof"))
			.screenEnclosureOnPremises.clickYesOrNo(hashMap.get("HomeDetailsIsthereascreenenclosureonpremises").equals("Yes"));
			plumbingSystemKnownLeaks.clickYesOrNo(hashMap.get("HomeDetailsDoestheplumbingsystemhaveanyknownleaks").equals("Yes"));

			return this;
		}

		public FLHO3AppHomeDetails selectValuationType(String valuationMethod)
		{
			return selectValuationTypeBase(valuationMethod);
		}
	}

	public class FLHO3AppWindMitigation extends ApplicationWindMitigationBase<FLHO3AppWindMitigation>
	{
	}

	public class FLHO3AppCoverages extends CoveragesBase<FLHO3AppCoverages>
	{
	}

	public class FLHO3AppDwellingProtectionDiscountsAndSurcharges extends ApplicationDwellingProtectionDiscountsAndSurchargesBase<FLHO3AppDwellingProtectionDiscountsAndSurcharges>
	{
		public GWRadioBinaryYesNo burglarBars = burglarBarsBase;
		public FLHO3AppDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public FLHO3AppDwellingProtectionDiscountsAndSurcharges setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(LinkedHashMap hashMap)
		{
			setAppDwellingProtectionDiscountsAndSurchargesFromHashMapBase(hashMap);

			/**
			 * Setting up values for later use in script
			 */
			Boolean burglarBars = hashMap.get("Dwellingprotection,discounts&surchargesArethereanyburglarbarsonthewindows/doors").equals("Yes");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			this.burglarBars.clickYesOrNo(burglarBars);
			if(burglarBars)
				safetyLatches.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesAretheresafetylatches").equals("Yes"));
			fireExtinguishers.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesOneormorefireextinguishersinthehome").equals("Yes"));
			deadbolts.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesDeadbolts").equals("Yes"));
			residenceVisibleToNeighbours.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesResidenceVisibletoNeighbors").equals("Yes"));

			return this;
		}
	}
}