package pageobjects;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SideBar
{
	WebDriver driver;
	GPAHelper gh;
	public QuotePreview quotePreview;
	private QuotePreview.ByQuotePreview by;

	public SideBar()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
	}
	public class QuotePreview
	{
		public final class ByQuotePreview
		{
			public final By printQuote = By.xpath("//button[contains(.,'Print Quote')]");
		}

		public void clickPrintQuote()
		{
			gh.clickElement(by.printQuote);
		}
	}
}