package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class GWRadioBinaryYesNo extends GWRadioBinary
{
	By valueLeft, valueRight;
	public GWRadioBinaryYesNo(By by)
	{
		super(by);
		valueLeft = new ByChained(by, By.cssSelector("input[id*='Left']"));
		valueRight = new ByChained(by, By.cssSelector("input[id*='Right']"));
	}

	public void clickYesOrNo(Boolean choice)
	{
		yesNoClickLogic(choice);
		if(getValue() != choice)
		{
			System.out.println("Failed to click yes or no");
			yesNoClickLogic(choice);
		}
	}

	private void yesNoClickLogic (Boolean choice)
	{
		Boolean startingValue = getValue();
		By yesNo = new ByChained(by, By.className("gw-radios-binary"));
		gh.waitForNoBackdrop();
		WebElement yesNoButton = driver.findElement(yesNo);
		if(startingValue != null && ((!startingValue && choice) || (startingValue && !choice)))
			gh.clickElement(yesNo);
		else if(startingValue == null && choice)
		{
			//Click Yes
			gh.wait(2).until(ExpectedConditions.elementToBeClickable(yesNoButton));
			new Actions(driver).moveToElement(yesNoButton, yesNoButton.getSize().getWidth()/4, yesNoButton.getSize().getHeight()/2).click().build().perform();
		}
		else if(startingValue == null && !choice)
		{
			//Click No
			gh.wait(2).until(ExpectedConditions.elementToBeClickable(yesNoButton));
			new Actions(driver).moveToElement(yesNoButton, yesNoButton.getSize().getWidth() - 15 , yesNoButton.getSize().getHeight()/2).click().build().perform();
		}
	}

	public Boolean getValue()
	{
		Boolean valueLeft = Boolean.parseBoolean(gh.getAttribute(this.valueLeft, "aria-checked")),
				valueRight = Boolean.parseBoolean(gh.getAttribute(this.valueRight, "aria-checked"));
		if(valueLeft == valueRight)
			return null;
		return valueLeft;
	}
}