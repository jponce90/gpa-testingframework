package pageobjects.SCHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.FLHO3.FLHO3SinglePageApplication;
import pageobjects.PossibleAccountMatches;

public class SCHO3
{
	public SCHO3SinglePageQuote quote;
	public SCHO3SinglePageApplication app;
	public PossibleAccountMatches possibleAccountMatches;

	public SCHO3()
	{
		quote = new SCHO3SinglePageQuote();
		app = new SCHO3SinglePageApplication();
		possibleAccountMatches = new PossibleAccountMatches();
	}

	public SCHO3SinglePageApplication continueAsNewCustomer()
	{
		possibleAccountMatches.clickContinueAsNewCustomer();
		new GPAHelper(LocalDriverManager.getDriver()).wait(30).until(ExpectedConditions.elementToBeClickable(app.applicantInfo.sectionBaseBy.sectionHeader));
		return app;
	}
}