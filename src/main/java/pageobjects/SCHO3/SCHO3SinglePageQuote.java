package pageobjects.SCHO3;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.WebDriver;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;
import pageobjects.QuoteSectionBases.QuoteDwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.QuoteSectionBases.QuoteHomeDetailsBase;
import pageobjects.QuoteSectionBases.QuoteWindMitigationBase;
import pageobjects.SinglePageQuoteBase;

import java.util.LinkedHashMap;

public class SCHO3SinglePageQuote extends SinglePageQuoteBase
{
	private WebDriver driver;
	private GPAHelper gh;
	public SCHO3QuoteHomeDetails homeDetails;
	public SCHO3QuoteWindMitigation windMitigation;
	public SCHO3QuoteCoverages coverages;
	public SCHO3QuoteDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;

	public SCHO3SinglePageQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		homeDetails = new SCHO3QuoteHomeDetails();
		windMitigation = new SCHO3QuoteWindMitigation();
		coverages = new SCHO3QuoteCoverages();
		dwellingProtectionAndSurcharges = new SCHO3QuoteDwellingProtectionDiscountsAndSurcharges();
	}

	public class SCHO3QuoteHomeDetails extends QuoteHomeDetailsBase<SCHO3QuoteHomeDetails>
	{
		public SCHO3QuoteHomeDetails showSection()
		{
			showSectionHomeDetails();
			return this;
		}

		public SCHO3QuoteHomeDetails setHomeDetailsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			setHomeDetailsFromHashMapBase(hashMap);

			setDwellingLimit(hashMap.get("HomeDetailsDwellingLimit"))
			.selectConstructionType(hashMap.get("HomeDetailsConstructionType"));
			if(hashMap.get("HomeDetailsDistancetoFireHydrant").equals("Within 1000 ft"))
				distanceToFireHydrant.setWithin1000ft();
			else
				distanceToFireHydrant.setOver1000ft();
			selectResidenceType(hashMap.get("HomeDetailsResidenceType"))
			.selectUsageType(hashMap.get("HomeDetailsUsageType"))
			.selectHowIsDwellingOccupied(hashMap.get("HomeDetailsHowisdwellingoccupied"));
			return this;
		}

		public SCHO3QuoteHomeDetails setDwellingLimit(String dwellingLimit)
		{
			return super.setDwellingLimit(dwellingLimit);
		}

		public String getDwellingLimit()
		{
			return super.getDwellingLimit();
		}
	}


	public class SCHO3QuoteWindMitigation extends QuoteWindMitigationBase<SCHO3QuoteWindMitigation>
	{
		public GWRadioBinaryYesNo roofCoverBuildingCodeCompliant = roofCoverBuildingCodeCompliantBase,
									roofWallBuildingCodeCompliant = roofWallBuildingCodeCompliantBase,
									secondaryWaterResistance = secondaryWaterResistanceBase;

		public SCHO3QuoteWindMitigation showSection()
		{
			super.showSectionWindMitigation();
			return this;
		}

		public SCHO3QuoteWindMitigation setWindMitigationFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection()
			.setRoofYear(hashMap.get("WindMitigationRoofYear"))
			.selectRoofType(hashMap.get("WindMitigationRoofType"))
			.selectRoofShape(hashMap.get("WindMitigationRoofShape"));
			roofCoverBuildingCodeCompliant.clickYesOrNo(hashMap.get("WindMitigationIstheroofcoverconstructionbuildingcodecompliant").equals("Yes"));
			roofWallBuildingCodeCompliant.clickYesOrNo(hashMap.get("WindMitigationIstheroofwallconnectionbuildingcodecompliant").equals("Yes"));
			secondaryWaterResistance.clickYesOrNo(hashMap.get("WindMitigationSecondaryWaterResistance").equals("Yes"));
			if(!hashMap.get("WindMitigationOpeningProtection").contains("not"))
				selectOpeningProtection(hashMap.get("WindMitigationOpeningProtection"));

			return this;
		}
	}

	public class SCHO3QuoteCoverages extends CoveragesBase<SCHO3QuoteCoverages>
	{
		public SCHO3SinglePageApplication continueToApp()
		{
			super.clickContinueToApplication();
			return new SCHO3SinglePageApplication();
		}

		public SCHO3QuoteCoverages clickMoreCoverage()
		{
			super.clickMoreCoverageBase();
			return this;
		}

		public SCHO3QuoteCoverages clickMostPopular()
		{
			super.clickMostPopularBase();
			return this;
		}

		public SCHO3QuoteCoverages clickLessCoverage()
		{
			super.clickLessCoverageBase();
			return this;
		}
	}

	public class SCHO3QuoteDwellingProtectionDiscountsAndSurcharges extends QuoteDwellingProtectionDiscountsAndSurchargesBase<SCHO3QuoteDwellingProtectionDiscountsAndSurcharges>
	{
		public SCHO3QuoteDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public SCHO3QuoteDwellingProtectionDiscountsAndSurcharges setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean fireAlarm = hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";")[0].equals("Yes"),
					fireSprinklers = hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";")[0].equals("Yes"),
					burglarAlarm = hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";")[0].equals("Yes");

			showSection();

			this.burglarAlarm.clickYesOrNo(burglarAlarm);
			if(burglarAlarm)
				selectBurglarAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";[ ]{0,1}")[1]);
			this.guardedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGuardedCommunity").equals("Yes"));
			this.gatedCommunity.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesGatedCommunity").equals("Yes"));
			this.fireAlarm.clickYesOrNo(fireAlarm);
			if(fireAlarm)
				selectFireAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";[ ]{0,1}")[1]);
			this.smokeAlarms.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSmokeAlarms").equals("Yes"));
			this.fireSprinklers.clickYesOrNo(fireSprinklers);
			if(fireSprinklers)
				selectFireSprinklersType("Full");
				//TODO input just says 'enter Sprinkler type'
//				selectFireSprinklersType(hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";[ ]{0,1}")[1]);
			return this;
		}
	}
}