package pageobjects.SCHO3;

import pageobjects.ApplicationSectionBases.*;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class SCHO3SinglePageApplication
{
	public SCHO3AppApplicantInfo applicantInfo;
	public SCHO3AppHomeDetails homeDetails;
	public SCHO3AppWindMitigation windMitigation;
	public SCHO3AppCoverages coverages;
	public SCHO3AppQualification qualification;
	public SCHO3AppDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;
	public LossHistoryBase lossHistory;
	public BindPolicyBase bindPolicy;


	public SCHO3SinglePageApplication()
	{
		applicantInfo = new SCHO3AppApplicantInfo();
		homeDetails = new SCHO3AppHomeDetails();
		windMitigation = new SCHO3AppWindMitigation();
		coverages = new SCHO3AppCoverages();
		qualification = new SCHO3AppQualification();
		dwellingProtectionAndSurcharges = new SCHO3AppDwellingProtectionDiscountsAndSurcharges();
		lossHistory = new LossHistoryBase();
		bindPolicy = new BindPolicyBase();
	}
	/**The following classes hold methods that only affect fields
	 * that appear in the applications for SCHO3 exclusively*/

	public class SCHO3AppApplicantInfo extends ApplicationApplicantInfoBase<SCHO3AppApplicantInfo>
	{
		public SCHO3AppApplicantInfo showSection()
		{
			showSectionBase();
			return this;
		}
	}

	public class SCHO3AppQualification extends ApplicationQualificationBase<SCHO3AppQualification>
	{
		public SCHO3AppQualification showSection()
		{
			showSectionQualification();
			return this;
		}
	}

	public class SCHO3AppHomeDetails extends ApplicationHomeDetailsBase<SCHO3AppHomeDetails>
	{
		public SCHO3AppHomeDetails showSection()
		{
			super.showSectionHomeDetails();
			return this;
		}

		public SCHO3AppHomeDetails setAppHomeDetailFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			/**
			 * Show Home Details Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean inTheWindpool = hashMap.get("HomeDetailsInthewindpool").equals("Yes"),
					swimmingPool = hashMap.get("HomeDetailsIsthereaswimmingpool").equals("Yes"),
					secondaryHeating = hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"),
					animal = hashMap.get("HomeDetailsAnyanimalorexoticpets").equals("Yes");
			String wiringType = hashMap.get("HomeDetailsWiring"),
					wiringDescription = hashMap.get("HomeDetailsWiringtypedescription");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			selectValuationTypeBase(hashMap.get("HomeDetailsValuationType"))
			.setReplacementCost(hashMap.get("HomeDetailsEstimatedReplacementCost"))
			.setDistanceToFireStation(hashMap.get("HomeDetailsDistancetoFireStation"))
			.selectLocationType(hashMap.get("HomeDetailsLocationType"))
			.inTheWindPool.clickYesOrNo(inTheWindpool);
			if(hashMap.get("HomeDetailsDistancetoCoast").equals("Within 1000 ft"))
				distanceToCoast.setWithin1000ft();
			else
				distanceToCoast.setOver1000ft();
			//TODO set Purchase Date
			//TODO set Purchase Price
			//TODO set Market Value
			occupiedDaily.clickYesOrNo(hashMap.get("HomeDetailsOccupiedDaily").equals("Yes"));
			this.swimmingPool.clickYesOrNo(swimmingPool);
			if(swimmingPool)
			{
				selectPoolLocation(hashMap.get("HomeDetailsPoolLocation"))
				.poolFenced.clickYesOrNo(hashMap.get("HomeDetailsIsthepoolFenced").equals("Yes"));
				divingBoard.clickYesOrNo(hashMap.get("HomeDetailsIsthereadivingboard").equals("Yes"));
				poolSlide.clickYesOrNo(hashMap.get("HomeDetailsIsthereapoolslide").equals("Yes"));
			}
			trampoline.clickYesOrNo(hashMap.get("HomeDetailsIsthereatrampoline").equals("Yes"));
			animals.clickYesOrNo(animal);
			if(animal)
			{
				//TODO fill out animal information
			}

			golfCarts.clickYesOrNo(hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"));
			recreationVehicles.clickYesOrNo(hashMap.get("HomeDetailsAnyownedrecreationvehicles").equals("Yes"));
			selectHousekeepingCondition(hashMap.get("HomeDetailsHousekeepingcondition"));
			selectNumberOfUnits(hashMap.get("HomeDetailsNumberofUnits"));
			selectUnitInFireWalls(hashMap.get("HomeDetailsUnitsinFireWalls"));
			setNumberOfStories(hashMap.get("HomeDetailsNumberofStories"));
			setSquareFootage(hashMap.get("HomeDetailsSquareFootage"));
			selectFoundationType(hashMap.get("HomeDetailsFoundationType"));
			selectPrimaryHeating(hashMap.get("HomeDetailsPrimaryHeating"));
			secondaryHeatingSystem.clickYesOrNo(secondaryHeating);
			selectPlumbing(hashMap.get("HomeDetailsPlumbing"))
			.setPlumbingYear(hashMap.get("HomeDetailsPlumbingyear"))
			.setWaterHeaterYear(hashMap.get("HomeDetailsWaterHeaterYear"))
			.selectWiring(wiringType);

			if(wiringType.equals("Other") && !wiringDescription.contains("blank"))
				setWiringDescription(wiringDescription);

			selectElectricalSystem(hashMap.get("HomeDetailsElectricalSystem"))
			.selectConditionOfRoof(hashMap.get("HomeDetailsConditionofRoof"))
			.screenEnclosureOnPremises.clickYesOrNo(hashMap.get("HomeDetailsIsthereascreenenclosureonpremises").equals("Yes"));
			plumbingSystemKnownLeaks.clickYesOrNo(hashMap.get("HomeDetailsDoestheplumbingsystemhaveanyknownleaks").equals("Yes"));

			return this;
		}

		public SCHO3AppHomeDetails selectValuationType(String valuationMethod)
		{
			return selectValuationTypeBase(valuationMethod);
		}
	}

	public class SCHO3AppWindMitigation extends ApplicationWindMitigationBase<SCHO3AppWindMitigation>
	{
	}

	public class SCHO3AppCoverages extends CoveragesBase<SCHO3AppCoverages>
	{
	}

	public class SCHO3AppDwellingProtectionDiscountsAndSurcharges extends ApplicationDwellingProtectionDiscountsAndSurchargesBase<SCHO3AppDwellingProtectionDiscountsAndSurcharges>
	{
		public GWRadioBinaryYesNo burglarBars = super.burglarBarsBase;
		public SCHO3AppDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public SCHO3AppDwellingProtectionDiscountsAndSurcharges setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(LinkedHashMap hashMap)
		{
			/**
			 * Show Dwelling Protection Discounts And Surcharges Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean burglarBars = hashMap.get("Dwellingprotection,discounts&surchargesArethereanyburglarbarsonthewindows/doors").equals("Yes");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			lockedFence.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesIstherealockedfence").equals("Yes"));
			this.burglarBars.clickYesOrNo(burglarBars);
			if(burglarBars)
				safetyLatches.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesAretheresafetylatches").equals("Yes"));
			fireExtinguishers.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesOneormorefireextinguishersinthehome").equals("Yes"));
			deadbolts.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesDeadbolts").equals("Yes"));
			residenceVisibleToNeighbours.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesResidenceVisibletoNeighbors").equals("Yes"));

			return this;
		}
	}
}