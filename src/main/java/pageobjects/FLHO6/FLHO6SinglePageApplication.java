package pageobjects.FLHO6;

import pageobjects.ApplicationSectionBases.*;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.GWRadioBinaryYesNo;

import java.util.LinkedHashMap;

public class FLHO6SinglePageApplication extends FLHO6SinglePageQuote
{
	public FLHO6AppApplicantInfo applicantInfo;
	public FLHO6AppHomeDetails homeDetails;
	public FLHO6AppWindMitigation windMitigation;
	public FLHO6AppCoverages coverages;
	public FLHO6AppQualification qualification;
	public FLHO6AppDwellingProtectionDiscountsAndSurcharges dwellingProtectionAndSurcharges;
	public LossHistoryBase lossHistory;
	public BindPolicyBase bindPolicy;

	public FLHO6SinglePageApplication()
	{
		applicantInfo = new FLHO6AppApplicantInfo();
		homeDetails = new FLHO6AppHomeDetails();
		windMitigation = new FLHO6AppWindMitigation();
		coverages = new FLHO6AppCoverages();
		qualification = new FLHO6AppQualification();
		dwellingProtectionAndSurcharges = new FLHO6AppDwellingProtectionDiscountsAndSurcharges();
		lossHistory = new LossHistoryBase();
		bindPolicy = new BindPolicyBase();
	}
	/**The following classes hold methods that only affect fields
	 * that appear in the applications for FLHO6 exclusively*/

	public class FLHO6AppApplicantInfo extends ApplicationApplicantInfoBase<FLHO6AppApplicantInfo>
	{
		public FLHO6AppApplicantInfo showSection()
		{
			showSectionBase();
			return this;
		}
	}

	public class FLHO6AppQualification extends ApplicationQualificationBase<FLHO6AppQualification>
	{
		public FLHO6AppQualification showSection()
		{
			showSectionQualification();
			return this;
		}
	}

	public class FLHO6AppHomeDetails extends ApplicationHomeDetailsBase<FLHO6AppHomeDetails>
	{
		public FLHO6AppHomeDetails showSection()
		{
			super.showSectionHomeDetails();
			return this;
		}

		public FLHO6AppHomeDetails setAppHomeDetailFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			/**
			 * Show Home Details Section
			 */
			showSection();

			/**
			 * Setting up values for later use in script
			 */
			Boolean inTheWindpool = hashMap.get("HomeDetailsInthewindpool").equals("Yes"),
					swimmingPool = hashMap.get("HomeDetailsIsthereaswimmingpool").equals("Yes"),
					secondaryHeating = hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"),
					animal = hashMap.get("HomeDetailsAnyanimalorexoticpets").equals("Yes");
			String wiringType = hashMap.get("HomeDetailsWiring"),
					wiringDescription = hashMap.get("HomeDetailsWiringtypedescription");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			setReplacementCost(hashMap.get("HomeDetailsEstimatedReplacementCost"))
			.setDistanceToFireStation(hashMap.get("HomeDetailsDistancetoFireStation"))
			.selectLocationType(hashMap.get("HomeDetailsLocationType"))
			.inTheWindPool.clickYesOrNo(inTheWindpool);
			if(hashMap.get("HomeDetailsDistancetoCoast").equals("Within 1000 ft"))
				distanceToCoast.setWithin1000ft();
			else
				distanceToCoast.setOver1000ft();
			//TODO set Purchase Date
			//TODO set Purchase Price
			//TODO market value
			occupiedDaily.clickYesOrNo(hashMap.get("HomeDetailsOccupiedDaily").equals("Yes"));
			this.swimmingPool.clickYesOrNo(swimmingPool);
			if(swimmingPool)
			{
				selectPoolLocation(hashMap.get("HomeDetailsPoolLocation"))
				.poolFenced.clickYesOrNo(hashMap.get("HomeDetailsIsthepoolFenced").equals("Yes"));
				divingBoard.clickYesOrNo(hashMap.get("HomeDetailsIsthereadivingboard").equals("Yes"));
				poolSlide.clickYesOrNo(hashMap.get("HomeDetailsIsthereapoolslide").equals("Yes"));
			}
			trampoline.clickYesOrNo(hashMap.get("HomeDetailsIsthereatrampoline").equals("Yes"));
			animals.clickYesOrNo(animal);
			if(animal)
			{
				//TODO fill out animal information
			}

			golfCarts.clickYesOrNo(hashMap.get("HomeDetailsAnyownedgolfcarts").equals("Yes"));
			recreationVehicles.clickYesOrNo(hashMap.get("HomeDetailsAnyownedrecreationvehicles").equals("Yes"));
			selectHousekeepingCondition(hashMap.get("HomeDetailsHousekeepingcondition"));
			selectNumberOfUnits(hashMap.get("HomeDetailsNumberofUnits"));
			selectUnitInFireWalls(hashMap.get("HomeDetailsUnitsinFireWalls"));
			setNumberOfStories(hashMap.get("HomeDetailsNumberofStories"));
			setUnitFloor(hashMap.get("HomeDetailsUnitFloor"));
			setSquareFootage(hashMap.get("HomeDetailsSquareFootage"));
			selectFoundationType(hashMap.get("HomeDetailsFoundationType"));
			selectPrimaryHeating(hashMap.get("HomeDetailsPrimaryHeating"));
			secondaryHeatingSystem.clickYesOrNo(secondaryHeating);
			selectPlumbing(hashMap.get("HomeDetailsPlumbing"))
			.setPlumbingYear(hashMap.get("HomeDetailsPlumbingyear"))
			.setWaterHeaterYear(hashMap.get("HomeDetailsWaterHeaterYear"))
			.selectWiring(wiringType);

			if(wiringType.equals("Other") && !wiringDescription.contains("blank"))
				setWiringDescription(wiringDescription);

			selectElectricalSystem(hashMap.get("HomeDetailsElectricalSystem"))
			.selectConditionOfRoof(hashMap.get("HomeDetailsConditionofRoof"))
			.screenEnclosureOnPremises.clickYesOrNo(hashMap.get("HomeDetailsIsthereascreenenclosureonpremises").equals("Yes"));
			plumbingSystemKnownLeaks.clickYesOrNo(hashMap.get("HomeDetailsDoestheplumbingsystemhaveanyknownleaks").equals("Yes"));

			return this;
		}

		public FLHO6AppHomeDetails setUnitFloor(String unitFloor)
		{
			return super.setUnitFloor(unitFloor);
		}
	}

	public class FLHO6AppWindMitigation extends ApplicationWindMitigationBase<FLHO6AppWindMitigation>
	{
	}

	public class FLHO6AppCoverages extends CoveragesBase<FLHO6AppCoverages>
	{
	}

	public class FLHO6AppDwellingProtectionDiscountsAndSurcharges extends ApplicationDwellingProtectionDiscountsAndSurchargesBase<FLHO6AppDwellingProtectionDiscountsAndSurcharges>
	{
		public GWRadioBinaryYesNo burglarBars = super.burglarBarsBase;
		public FLHO6AppDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public FLHO6AppDwellingProtectionDiscountsAndSurcharges setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			setAppDwellingProtectionDiscountsAndSurchargesFromHashMapBase(hashMap);

			/**
			 * Setting up values for later use in script
			 */
			Boolean burglarBars = hashMap.get("Dwellingprotection,discounts&surchargesArethereanyburglarbarsonthewindows/doors").equals("Yes");

			/**
			 * Setting Values based on inputted LinkedHashMap
			 */
			this.burglarBars.clickYesOrNo(burglarBars);
			if(burglarBars)
				safetyLatches.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesAretheresafetylatches").equals("Yes"));
			fireExtinguishers.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesOneormorefireextinguishersinthehome").equals("Yes"));
			deadbolts.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesDeadbolts").equals("Yes"));
			residenceVisibleToNeighbours.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesResidenceVisibletoNeighbors").equals("Yes"));

			return this;
		}
	}
}