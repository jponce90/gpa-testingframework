package pageobjects.FLHO6;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageobjects.PossibleAccountMatches;
import pageobjects.SideBar;

public class FLHO6
{
	public FLHO6SinglePageQuote quote;
	public FLHO6SinglePageApplication app;
	public SideBar sideBar;
	private PossibleAccountMatches possibleAccountMatches;
	public FLHO6()
	{
		quote = new FLHO6SinglePageQuote();
		app = new FLHO6SinglePageApplication();
		possibleAccountMatches = new PossibleAccountMatches();
		sideBar = new SideBar();
	}

	public FLHO6SinglePageApplication continueAsNewCustomer()
	{
		possibleAccountMatches.clickContinueAsNewCustomer();
		new GPAHelper(LocalDriverManager.getDriver()).wait(30).until(ExpectedConditions.elementToBeClickable(app.applicantInfo.sectionBaseBy.sectionHeader));
		return app;
	}
}