package pageobjects.FLHO6;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.openqa.selenium.WebDriver;
import pageobjects.BaseSections.CoveragesBase;
import pageobjects.QuoteSectionBases.QuoteDwellingProtectionDiscountsAndSurchargesBase;
import pageobjects.QuoteSectionBases.QuoteHomeDetailsBase;
import pageobjects.QuoteSectionBases.QuoteWindMitigationBase;
import pageobjects.SinglePageQuoteBase;

import java.util.LinkedHashMap;

public class FLHO6SinglePageQuote extends SinglePageQuoteBase
{
	private WebDriver driver;
	private GPAHelper gh;
	public FLHO6QuoteHomeDetails homeDetails;
	public FLHO6QuoteWindMitigation windMitigation;
	public FLHO6QuoteDwellingProtectionDiscountsAndSurcharges dwellingProtectionDiscountsAndSurcharges;
	public FLHO6QuoteCoverages coverages;

	public FLHO6SinglePageQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		homeDetails = new FLHO6QuoteHomeDetails();
		windMitigation = new FLHO6QuoteWindMitigation();
		coverages = new FLHO6QuoteCoverages();
		dwellingProtectionDiscountsAndSurcharges = new FLHO6QuoteDwellingProtectionDiscountsAndSurcharges();
	}

	public class FLHO6QuoteHomeDetails extends QuoteHomeDetailsBase<FLHO6QuoteHomeDetails>
	{
		public FLHO6QuoteHomeDetails showSection()
		{
			showSectionHomeDetails();
			return this;
		}

		public FLHO6QuoteHomeDetails setHomeDetailsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			showSection();

			setHomeDetailsFromHashMapBase(hashMap);

			setDwellingLimit(hashMap.get("HomeDetailsDwellingLimit"))
			.setPersonalProperty(hashMap.get("HomeDetailsPersonalProperty"))
			.selectConstructionType(hashMap.get("HomeDetailsConstructionType"));
			if(hashMap.get("HomeDetailsDistancetoFireHydrant").equals("Within 1000 ft"))
				distanceToFireHydrant.setWithin1000ft();
			else
				distanceToFireHydrant.setOver1000ft();
			selectResidenceType(hashMap.get("HomeDetailsResidenceType"))
			.selectUsageType(hashMap.get("HomeDetailsUsageType"))
			.selectHowIsDwellingOccupied(hashMap.get("HomeDetailsHowisdwellingoccupied"));
			return this;
		}

		public FLHO6QuoteHomeDetails setDwellingLimit(String dwellingLimit)
		{
			return super.setDwellingLimit(dwellingLimit);
		}

		public String getDwellingLimit()
		{
			return super.getDwellingLimit();
		}

		public FLHO6QuoteHomeDetails setPersonalProperty(String personalProperty)
		{
			return super.setPersonalProperty(personalProperty);
		}

		public String getPersonalProperty()
		{
			return super.getPersonalProperty();
		}
	}

	public class FLHO6QuoteWindMitigation extends QuoteWindMitigationBase<FLHO6QuoteWindMitigation>
	{
		public FLHO6QuoteWindMitigation showSection()
		{
			super.showSectionWindMitigation();
			return this;
		}

		public FLHO6QuoteWindMitigation setWindMitigationFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			setWindMitigationFromHashMapBase(hashMap);

			setRoofYear(hashMap.get("WindMitigationRoofYear"))
			.selectRoofType(hashMap.get("WindMitigationRoofType"))
			.selectRoofShape(hashMap.get("WindMitigationRoofShape"))
			.secondaryWaterResistanceBase.clickYesOrNo(hashMap.get("WindMitigationSecondaryWaterResistance").equals("Yes"));
			if(!hashMap.get("WindMitigationOpeningProtection").contains("not"))
				selectOpeningProtection(hashMap.get("WindMitigationOpeningProtection"));

			int yearBuilt = Integer.parseInt(hashMap.get("HomeDetailsYearbuilt"));
			if(yearBuilt < 2002)
				selectRoofCover(hashMap.get("WindMitigationRoofCover"));
			//TODO IF ROOF DECK ATTACHMENT
			if(yearBuilt < 2002)
				selectRoofWallConnection(hashMap.get("WindMitigationRoofWallConnection"));
			if(yearBuilt> 2001)
				selectRoofDeck(hashMap.get("WindMitigationRoofDeck"));
			//TODO PREFILLS

			return this;
		}
	}

	public class FLHO6QuoteDwellingProtectionDiscountsAndSurcharges extends QuoteDwellingProtectionDiscountsAndSurchargesBase<FLHO6QuoteDwellingProtectionDiscountsAndSurcharges>
	{
		public FLHO6QuoteDwellingProtectionDiscountsAndSurcharges showSection()
		{
			showSectionDwellingProtectionDiscountsAndSurcharges();
			return this;
		}

		public FLHO6QuoteDwellingProtectionDiscountsAndSurcharges setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
		{
			Boolean fireAlarm = hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";")[0].equals("Yes"),
					fireSprinklers = hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";")[0].equals("Yes"),
					burglarAlarm = hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";")[0].equals("Yes");

			setDwellingProtectionDiscountsAndSurchargesFromHashMapBase(hashMap);

			this.burglarAlarm.clickYesOrNo(burglarAlarm);
			if(burglarAlarm)
				selectBurglarAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesBurglarAlarm").split(";[ ]{0,1}")[1]);
			this.fireAlarm.clickYesOrNo(fireAlarm);
			if(fireAlarm)
				selectFireAlarmType(hashMap.get("Dwellingprotection,discounts&surchargesFireAlarm").split(";[ ]{0,1}")[1]);
			this.smokeAlarms.clickYesOrNo(hashMap.get("Dwellingprotection,discounts&surchargesSmokeAlarms").equals("Yes"));
			this.fireSprinklers.clickYesOrNo(fireSprinklers);
			if(fireSprinklers)
				selectFireSprinklersType("Full");
				//TODO input just says 'enter Sprinkler type'
//				selectFireSprinklersType(hashMap.get("Dwellingprotection,discounts&surchargesFireSprinklers").split(";[ ]{0,1}")[1]);
			return this;
		}
	}

	public class FLHO6QuoteCoverages extends CoveragesBase<FLHO6QuoteCoverages>
	{
		public FLHO6SinglePageApplication continueToApp()
		{
			super.clickContinueToApplication();
			return new FLHO6SinglePageApplication();
		}

		public FLHO6QuoteCoverages clickMoreCoverage()
		{
			super.clickMoreCoverageBase();
			return this;
		}

		public FLHO6QuoteCoverages clickMostPopular()
		{
			super.clickMostPopularBase();
			return this;
		}

		public FLHO6QuoteCoverages clickLessCoverage()
		{
			super.clickLessCoverageBase();
			return this;
		}
	}

}