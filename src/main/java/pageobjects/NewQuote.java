package pageobjects;

import helpers.GPAHelper;
import helpers.LocalDriverManager;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.LinkedHashMap;

public class NewQuote
{
	public final GPAHelper gh;
	public final GPAHelper.SelectHelper ghs;
	public final WebDriver driver;
	public final NewQuoteBy by = new NewQuoteBy();
	public String pageUrl = null;
	public final GWRadioBinaryYesNo areThereOtherPoliciesWithFrontline = new GWRadioBinaryYesNo(By.cssSelector("[label='Are there any other policies with Frontline?']"));

	public NewQuote()
	{
		driver = LocalDriverManager.getDriver();
		gh = new GPAHelper(driver);
		ghs = gh.new SelectHelper();
		pageUrl = "http://10.50.50."+LocalDriverManager.getSessionInfo().environment+"/gateway-portal/html/index.html#/new-quote/";
	}

	public final void get()
	{
		driver.get(pageUrl);
		gh.wait(10)
		.until(ExpectedConditions
		.and(ExpectedConditions.invisibilityOfElementLocated(By.className("app-loading")),
		ExpectedConditions.elementToBeClickable(by.producerCode)));
	}

	public final class NewQuoteBy
	{
		public final By producerCode = By.id("ProducerCode"),
						riskState = By.cssSelector("[gw-pl-select='newSubmissionView.state.value']"),
						policyType = By.cssSelector("[gw-pl-select='newSubmissionView.policyType.value']"),
						policyWriter = By.id("PolicyWriter"),
						effectiveDate = By.id("EffectiveDate"),
						areThereAnyOtherPoliciesWithFrontline = By.cssSelector("#binaryinputctrlx0Right~div.gw-labels"),
						firstName = By.cssSelector("[label='First Name'] input"),
						lastName = By.cssSelector("[label='Last Name'] input"),
						locationAddress1 = By.cssSelector("[label='Location Address 1'] input"),
						locationAddress2 = By.cssSelector("[label='Location Address 2'] input"),
						city = By.cssSelector("[label='City'] input"),
						county = By.cssSelector("[label='County'] input"),
						zipCode = By.cssSelector("[label = 'ZIP Code'] input"),
						verifyAddress = By.cssSelector(".address-verify-module-fli>button"),
						next = By.className("gw-next");
	}

	public NewQuote setNewQuoteFieldsFromHashMap(LinkedHashMap<String, String> hashMap)
	{
		int daysInFuture = Integer.parseInt(hashMap.get("NewQuoteDetailsEffectiveDate"));
		org.joda.time.format.DateTimeFormatter formatter = org.joda.time.format.DateTimeFormat.forPattern("MM/dd/YYYY");
		DateTime dayInFuture = new DateTime().plusDays(daysInFuture);

		this
		.selectProducerCode("-- Choose Producer Code--")
		.selectProducerCode(hashMap.get("NewQuoteDetailsProducerCode"))
		.selectRiskState(hashMap.get("NewQuoteDetailsRiskState"))
		.selectPolicyType(hashMap.get("NewQuoteDetailsPolicyType"))
		//TODO Hash map refers to Holly Allaire but only User4 Acentria is available
//		.selectPolicyWriter(hashMap.get("NewQuoteDetailsPolicyWriter"))
		.selectPolicyWriter("User4 Acentria")
		.setEffectiveDate(formatter.print(dayInFuture))
		.setFirstName("HappyPathFirstName")
		.setLastName("LastName")
		.setLocationAddress1(hashMap.get("NewQuoteDetailsLocationAddress1"))
		.setCity(hashMap.get("NewQuoteDetailsCity"))
		.setZipCode(hashMap.get("NewQuoteDetailsZipCode"))
		.areThereOtherPoliciesWithFrontline.clickYesOrNo(hashMap.get("NewQuoteDetailsArethereanyotherpolicieswithFrontline").equals("Yes"));

		if(getRiskState().equals("Alabama") || getRiskState().equals("South Carolina"))
			setCounty(hashMap.get("NewQuoteDetailsCounty"));

		clickVerifyAddress();
		gh.waitForNoBackdrop();
		if(driver.findElements(By.xpath("//span[contains(.,'Address Verified')]")).size()==0)
			driver.findElement(By.cssSelector(".address-verify-module-fli table tbody tr:nth-of-type(1) button")).click();
		return this;
	}

	public String getProducerCode()
	{
		return ghs.getSelectedOption(by.producerCode);
	}

	public NewQuote selectProducerCode(String producerCode)
	{
		ghs.selectOption(by.producerCode, producerCode);
		return this;
	}

	public String getRiskState()
	{
		return ghs.getSelectedOption(by.riskState);
	}

	public NewQuote selectRiskState(String riskState)
	{
		ghs.assertSelectedOption(by.riskState, "-- Choose State--");
		ghs.selectOption(by.riskState, riskState);
		return this;
	}

	public String getPolicyType()
	{
		return ghs.getSelectedOption(by.policyType);
	}

	public NewQuote selectPolicyType(String policyType)
	{
		ghs.selectOption(by.policyType, policyType);
		return this;
	}

	public String getPolicyWriter()
	{
		return ghs.getSelectedOption(by.policyWriter);
	}

	public NewQuote selectPolicyWriter(String policyWriter)
	{
		ghs.assertSelectedOption(by.policyWriter, "-- Choose Policy Writer --");
		ghs.selectOption(by.policyWriter, policyWriter);
		return this;
	}

	public String getEffectiveDate()
	{
		return gh.getValue(by.effectiveDate);
	}

	public NewQuote setEffectiveDate(String effectiveDate)
	{
		By datePicker = By.className("gw-open");
		gh.setText(by.effectiveDate, effectiveDate);
		if(gh.isDisplayed(datePicker))
		{
			gh.clickElement(by.effectiveDate);
			gh.wait(2).until(ExpectedConditions.invisibilityOfElementLocated(datePicker));
		}
		return this;
	}
	
	//TODO: this field needs some more work
	public String getAreThereAnyOtherPoliciesWithFrontline()
	{
		return gh.getAttribute(By.id("binaryinputctrlx0Left"), "aria-checked").equals("true") ? "Yes" : "No";
	}
	
	//TODO: insured type
	
	public String getFirstName()
	{
		return gh.getValue(by.firstName);
	}
	
	public NewQuote setFirstName(String firstName)
	{
		gh.setText(by.firstName, firstName);
		return this;
	}
	
	public String getLastName()
	{
		return gh.getValue(by.lastName);
	}
	
	public NewQuote setLastName(String lastName)
	{
		gh.setText(by.lastName, lastName);
		return this;
	}

	public String getLocationAddress1()
	{
		return gh.getValue(by.locationAddress1);
	}

	public NewQuote setLocationAddress1(String locationAddress1)
	{
		gh.setText(by.locationAddress1, locationAddress1);
		return this;
	}

	public String getLocationAddress2()
	{
		return gh.getValue(by.locationAddress2);
	}

	public NewQuote setLocationAddress2(String locationAddress2)
	{
		gh.setText(by.locationAddress2, locationAddress2);
		return this;
	}

	public String getCity()
	{
		return gh.getValue(by.city);
	}

	public NewQuote setCity(String city)
	{
		gh.setText(by.city, city);
		return this;
	}

	public NewQuote setCounty(String county)
	{
		gh.setText(by.county, county);
		return this;
	}

	public String getZipCode()
	{
		return gh.getValue(by.zipCode);
	}

	public NewQuote setZipCode(String zipCode)
	{
		gh.setText(by.zipCode, zipCode);
		return this;
	}

	public void clickVerifyAddress()
	{
		gh.clickElement(by.verifyAddress);
	}

	public NewQuote verifyAddress()
	{
		clickVerifyAddress();
		gh.wait(10).until(ExpectedConditions.visibilityOfElementLocated(By.className("gw-modal-backdrop")));
		gh.wait(20).until(ExpectedConditions.invisibilityOfElementLocated(By.className("gw-modal-backdrop")));
//		gh.wait(10).until(ExpectedConditions.visibilityOfElementLocated(By.className("success-match-fli")));
		gh.wait(10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(.,'Address Verified')]")));
//		gh.wait(10).until(ExpectedConditions.textToBe(By.className("success-match-fli"), "Address Verified"));

		return this;
	}

	public void clickNext()
	{
		gh.clickElement(by.next);
	}

	public void next()
	{
		clickNext();
		gh.wait(60).until(ExpectedConditions.urlToBe("http://10.50.50."+LocalDriverManager.getSessionInfo().environment+"/gateway-portal/html/index.html#/qnb-flow/"));
		gh.wait(30).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("class*='backdrop'")));
	}
}