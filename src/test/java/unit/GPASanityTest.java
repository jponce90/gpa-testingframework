package unit;

import base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageobjects.*;
import pageobjects.ALHO3.ALHO3;
import pageobjects.ALHO3.ALHO3SinglePageApplication;
import pageobjects.FLHO3.FLHO3;
import pageobjects.FLHO3.FLHO3SinglePageApplication;
import pageobjects.FLHO6.FLHO6;
import pageobjects.FLHO6.FLHO6SinglePageApplication;
import pageobjects.NCHO3.NCHO3;
import pageobjects.NCHO3.NCHO3SinglePageApplication;
import pageobjects.SCHO3.SCHO3;
import pageobjects.SCHO3.SCHO3SinglePageApplication;

import static org.testng.Assert.assertEquals;

@Test
public class GPASanityTest extends BaseTest
{
	NewQuote nq;
	String policyWriter = "Holly Allaire",
			effectiveDate = "02/20/2017";

	@BeforeMethod
	public void beforeMethod()
	{
		setupDriver();
		nq = new NewQuote();
		nq.get();
//		nq.gh.new ModalHelper().clickCancel();
	}

	@AfterMethod
	public void afterMethod(ITestResult iTestResult)
	{
		if(iTestResult.isSuccess())
			nq.driver.quit();
	}

	@Test(enabled = false)
	public void testSwitcher()
	{
		String producerCode = "523-23-21388 Acentria, Inc. (MAIN)",
				riskState = "Florida",
				policyType = "Homeowners (HO3)",
				effectiveDate = "01/01/2016",
				firstName = "testFirstName",
				lastName = "testLastName",
				locationAddress1 = "2470 Wild Wood Dr",
				city = "Melbourne",
				zipCode = "32935";

		nq
		.selectProducerCode(producerCode)
		.selectRiskState(riskState)
		.selectPolicyType(policyType)
		.selectPolicyWriter(policyWriter)
		.setEffectiveDate(effectiveDate)
		.setFirstName(firstName)
		.setLastName(lastName)
		.setLocationAddress1(locationAddress1)
		.setCity(city)
		.setZipCode(zipCode);

		By yesInput = By.id("binaryinputctrlx0Left"),
			noInput = By.id("binaryinputctrlx0Right"),
			yesLabel = new ByChained(nq.by.areThereAnyOtherPoliciesWithFrontline, By.cssSelector("[title='Yes']")),
			noLabel = new ByChained(nq.by.areThereAnyOtherPoliciesWithFrontline, By.cssSelector("[title='No']"));

		nq.gh.clickElement(noLabel);
	}

	public void newFLHO3QuoteDetails()
	{
		String producerCode = "523-23-21388 Acentria, Inc. (MAIN)",
				riskState = "Florida",
				policyType = "Homeowners (HO3)",
				areThereAnyOtherPoliciesWithFrontline = "No",
				firstName = "testFirstName",
				lastName = "testLastName",
				locationAddress1 = "2470 Wild Wood Dr",
				city = "Melbourne",
				zipCode = "32935";
		nq
		.selectProducerCode("-- Choose Producer Code--")
		.selectProducerCode(producerCode)
		.selectRiskState(riskState)
		.selectPolicyType(policyType)
		.selectPolicyWriter(policyWriter)
		.setEffectiveDate(effectiveDate)
//		.setAreThereAnyOtherPoliciesWithFrontline(areThereAnyOtherPoliciesWithFrontline)
		.setFirstName(firstName)
		.setLastName(lastName)
		.setLocationAddress1(locationAddress1)
		.setCity(city)
		.setZipCode(zipCode);

		assertEquals(nq.getProducerCode(), producerCode);
		assertEquals(nq.getRiskState(), riskState);
		assertEquals(nq.getPolicyType(), policyType);
		assertEquals(nq.getPolicyWriter(), policyWriter);
		assertEquals(nq.getEffectiveDate(), effectiveDate);
//		assertEquals(nq.getAreThereAnyOtherPoliciesWithFrontline(), areThereAnyOtherPoliciesWithFrontline);
		assertEquals(nq.getFirstName(), firstName);
		assertEquals(nq.getLastName(), lastName);
		assertEquals(nq.getLocationAddress1(), locationAddress1);
		assertEquals(nq.getCity(), city);
		assertEquals(nq.getZipCode(), zipCode);

		nq.verifyAddress();
		nq.next();

		String dwellingLimit = "300000",
				yearBuilt = "2005",
				constructionType = "Masonry";

		FLHO3 flho3 = new FLHO3();

		//Home Details Quote Area
		flho3.quote.homeDetails.showSection()
		.setDwellingLimit(dwellingLimit)
		.setYearBuilt(yearBuilt)
		.selectConstructionType(constructionType)
		.distanceToFireHydrant.setOver1000ft();

		assertEquals(flho3.quote.homeDetails.getDwellingLimit(), dwellingLimit);
		assertEquals(flho3.quote.homeDetails.getYearBuilt(), yearBuilt);
		assertEquals(flho3.quote.homeDetails.getConstructionType(), constructionType);
		assertEquals(flho3.quote.homeDetails.distanceToFireHydrant.getDistanceToFireHydrant(), "Over 1000ft");

		//Wind Mitigation Area
		String roofYear = "2005",
				roofType = "Asphalt Shingle",
				roofShape = "Hip";
		flho3.quote.windMitigation.showSection()
		.setRoofYear(roofYear)
		.selectRoofType(roofType)
		.selectRoofShape(roofShape);

		assertEquals(flho3.quote.windMitigation.getRoofYear(), roofYear);
		assertEquals(flho3.quote.windMitigation.getRoofType(), roofType);
		assertEquals(flho3.quote.windMitigation.getRoofShape(), roofShape);

		//Coverages Area
		flho3.quote.coverages
		.showSectionIgnoreValidationWarning();
		flho3.quote.coverages
		.continueToApp();

		//Entering Application Phase
		FLHO3SinglePageApplication app = flho3.continueAsNewCustomer();

		//Applicant Info Area
		String dateOfBirth = "02/04/1990",
				SSN = "123-23-1234",
				homePhone = "407-123-1234";
		app.applicantInfo.showSection()
		.setDateOfBirth(dateOfBirth)
		.setSSN(SSN)
		.setHomePhone(homePhone)
		.setHomePhoneAsPrimary();

		//Home Details Section
		String valuationType = "Appraisal",
				replacementCost = "300000",
				units = "2",
				numberOfStories = "4",
				squareFootage = "2000",
				foundationType = "Open",
				primaryHeating = "Electric",
				plumbing = "PVC",
				wiring = "Copper",
				electricalSystem = "Circuit Breaker",
				conditionOfRoof = "Average";
		app.homeDetails.showSection()
		.selectValuationType(valuationType)
		.setReplacementCost(replacementCost)
		.selectUnitInFireWalls(units)
		.setNumberOfStories(numberOfStories)
		.setSquareFootage(squareFootage)
		.selectFoundationType(foundationType)
		.selectPrimaryHeating(primaryHeating)
		.selectPlumbing(plumbing)
		.selectWiring(wiring)
		.selectElectricalSystem(electricalSystem)
		.selectConditionOfRoof(conditionOfRoof)
		.secondaryHeatingSystem.clickYesOrNo(false);
		app.homeDetails.distanceToCoast
		.toggleRadio();
		app.homeDetails.screenEnclosureOnPremises
		.clickYesOrNo(true);
		app.homeDetails.inTheWindPool.clickYesOrNo(true);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.showSection()
		.lockedFence.clickYesOrNo(true);
		app.dwellingProtectionAndSurcharges.burglarBars.clickYesOrNo(false);

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	public void newFLHO6QuoteDetails()
	{
		String producerCode = "523-23-21388 Acentria, Inc. (MAIN)",
				riskState = "Florida",
				policyType = "Condominium (HO6)",
				areThereAnyOtherPoliciesWithFrontline = "No",
				firstName = "testFirstName",
				lastName = "testLastName",
				locationAddress1 = "2470 Wild Wood Dr",
				city = "Melbourne",
				zipCode = "32935";
		nq
		.selectProducerCode("-- Choose Producer Code--")
		.selectProducerCode(producerCode)
		.selectRiskState(riskState)
		.selectPolicyType(policyType)
		.selectPolicyWriter(policyWriter)
		.setEffectiveDate(effectiveDate)
//		.setAreThereAnyOtherPoliciesWithFrontline(areThereAnyOtherPoliciesWithFrontline)
		.setFirstName(firstName)
		.setLastName(lastName)
		.setLocationAddress1(locationAddress1)
		.setCity(city)
		.setZipCode(zipCode);

		assertEquals(nq.getProducerCode(), producerCode);
		assertEquals(nq.getRiskState(), riskState);
		assertEquals(nq.getPolicyType(), policyType);
		assertEquals(nq.getPolicyWriter(), policyWriter);
		assertEquals(nq.getEffectiveDate(), effectiveDate);
//		assertEquals(nq.getAreThereAnyOtherPoliciesWithFrontline(), areThereAnyOtherPoliciesWithFrontline);
		assertEquals(nq.getFirstName(), firstName);
		assertEquals(nq.getLastName(), lastName);
		assertEquals(nq.getLocationAddress1(), locationAddress1);
		assertEquals(nq.getCity(), city);
		assertEquals(nq.getZipCode(), zipCode);

		nq.verifyAddress();
		nq.next();

		String dwellingLimit = "300000",
				personalProperty = "150000",
				yearBuilt = "2005",
				constructionType = "Masonry";

		FLHO6 flho6 = new FLHO6();

		//Home Details Quote Area
		flho6.quote.homeDetails.showSection()
		.setDwellingLimit(dwellingLimit)
		.setPersonalProperty(personalProperty)
		.setYearBuilt(yearBuilt)
		.selectConstructionType(constructionType)
		.distanceToFireHydrant.setOver1000ft();

		assertEquals(flho6.quote.homeDetails.getDwellingLimit(), dwellingLimit);
		assertEquals(flho6.quote.homeDetails.getYearBuilt(), yearBuilt);
		assertEquals(flho6.quote.homeDetails.getConstructionType(), constructionType);
		assertEquals(flho6.quote.homeDetails.distanceToFireHydrant.getDistanceToFireHydrant(), "Over 1000ft");

		//Wind Mitigation Area
		String roofYear = "2005",
				roofType = "Asphalt Shingle",
				roofShape = "Hip";
		flho6.quote.windMitigation.showSection()
		.setRoofYear(roofYear)
		.selectRoofType(roofType)
		.selectRoofShape(roofShape);

		assertEquals(flho6.quote.windMitigation.getRoofYear(), roofYear);
		assertEquals(flho6.quote.windMitigation.getRoofType(), roofType);
		assertEquals(flho6.quote.windMitigation.getRoofShape(), roofShape);

		//Coverages Area
		flho6.quote.coverages
		.showSectionIgnoreValidationWarning();
		flho6.quote.coverages
		.continueToApp();

		//Entering Application Phase
		FLHO6SinglePageApplication app = flho6.continueAsNewCustomer();

		//Applicant Info Area
		String dateOfBirth = "02/04/1990",
				SSN = "123-23-1234",
				homePhone = "407-123-1234";
		app.applicantInfo.showSection()
		.setDateOfBirth(dateOfBirth)
		.setSSN(SSN)
		.setHomePhone(homePhone)
		.setHomePhoneAsPrimary();

		//Home Details Section
		String valuationType = "Appraisal",
				replacementCost = "300000",
				units = "2",
				numberOfStories = "4",
				unitFloor = "2",
				squareFootage = "2000",
				foundationType = "Open",
				primaryHeating = "Electric",
				plumbing = "PVC",
				wiring = "Copper",
				electricalSystem = "Circuit Breaker",
				conditionOfRoof = "Average";
		app.homeDetails.showSection()
		.setReplacementCost(replacementCost)
		.selectUnitInFireWalls(units)
		.setNumberOfStories(numberOfStories)
		.setUnitFloor(unitFloor)
		.setSquareFootage(squareFootage)
		.selectFoundationType(foundationType)
		.selectPrimaryHeating(primaryHeating)
		.selectPlumbing(plumbing)
		.selectWiring(wiring)
		.selectElectricalSystem(electricalSystem)
		.selectConditionOfRoof(conditionOfRoof)
		.secondaryHeatingSystem.clickYesOrNo(false);
		app.homeDetails.distanceToCoast
		.toggleRadio();
		app.homeDetails.screenEnclosureOnPremises
		.clickYesOrNo(true);
		app.homeDetails.inTheWindPool.clickYesOrNo(true);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.showSection()
		.lockedFence.clickYesOrNo(true);
		app.dwellingProtectionAndSurcharges.burglarBars.clickYesOrNo(false);

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	public void newSCHO3QuoteDetails()
	{
		String producerCode = "523-23-21388 Acentria, Inc. (MAIN)",
				riskState = "South Carolina",
				policyType = "Homeowners (HO3)",
				areThereAnyOtherPoliciesWithFrontline = "No",
				firstName = "testFirstName",
				lastName = "testLastName",
				locationAddress1 = "32 Legare St",
				city = "Charleston",
				zipCode = "29401";
		nq
		.selectProducerCode("-- Choose Producer Code--")
		.selectProducerCode(producerCode)
		.selectRiskState(riskState)
		.selectPolicyType(policyType)
		.selectPolicyWriter(policyWriter)
		.setEffectiveDate(effectiveDate)
//		.setAreThereAnyOtherPoliciesWithFrontline(areThereAnyOtherPoliciesWithFrontline)
		.setFirstName(firstName)
		.setLastName(lastName)
		.setLocationAddress1(locationAddress1)
		.setCity(city)
		.setZipCode(zipCode);

		assertEquals(nq.getProducerCode(), producerCode);
		assertEquals(nq.getRiskState(), riskState);
		assertEquals(nq.getPolicyType(), policyType);
		assertEquals(nq.getPolicyWriter(), policyWriter);
		assertEquals(nq.getEffectiveDate(), effectiveDate);
//		assertEquals(nq.getAreThereAnyOtherPoliciesWithFrontline(), areThereAnyOtherPoliciesWithFrontline);
		assertEquals(nq.getFirstName(), firstName);
		assertEquals(nq.getLastName(), lastName);
		assertEquals(nq.getLocationAddress1(), locationAddress1);
		assertEquals(nq.getCity(), city);
		assertEquals(nq.getZipCode(), zipCode);

		nq.verifyAddress();
		nq.next();

		String dwellingLimit = "300000",
				yearBuilt = "2005",
				constructionType = "Masonry";

		SCHO3 scho3 = new SCHO3();

		//Home Details Quote Area
		scho3.quote.homeDetails.showSection()
		.setDwellingLimit(dwellingLimit)
		.setYearBuilt(yearBuilt)
		.selectConstructionType(constructionType)
		.distanceToFireHydrant.setOver1000ft();

		assertEquals(scho3.quote.homeDetails.getDwellingLimit(), dwellingLimit);
		assertEquals(scho3.quote.homeDetails.getYearBuilt(), yearBuilt);
		assertEquals(scho3.quote.homeDetails.getConstructionType(), constructionType);
		assertEquals(scho3.quote.homeDetails.distanceToFireHydrant.getDistanceToFireHydrant(), "Over 1000ft");

		//Wind Mitigation Area
		String roofYear = "2005",
				roofType = "Asphalt Shingle",
				roofShape = "Hip";
		scho3.quote.windMitigation.showSection()
		.setRoofYear(roofYear)
		.selectRoofType(roofType)
		.selectRoofShape(roofShape);

		assertEquals(scho3.quote.windMitigation.getRoofYear(), roofYear);
		assertEquals(scho3.quote.windMitigation.getRoofType(), roofType);
		assertEquals(scho3.quote.windMitigation.getRoofShape(), roofShape);

		//Coverages Area
		scho3.quote.coverages
		.showSection();
		scho3.quote.coverages
		.continueToApp();

		//Entering Application Phase
		SCHO3SinglePageApplication app = scho3.continueAsNewCustomer();

		//Applicant Info Area
		String dateOfBirth = "02/04/1990",
				SSN = "123-23-1234",
				homePhone = "407-123-1234";
		app.applicantInfo.showSection()
		.setDateOfBirth(dateOfBirth)
		.setSSN(SSN)
		.setHomePhone(homePhone)
		.setHomePhoneAsPrimary();

		//Home Details Section
		String valuationType = "Appraisal",
				replacementCost = "300000",
				units = "2",
				numberOfStories = "4",
				squareFootage = "2000",
				foundationType = "Open",
				primaryHeating = "Electric",
				plumbing = "PVC",
				wiring = "Copper",
				electricalSystem = "Circuit Breaker",
				conditionOfRoof = "Average";
		app.homeDetails.showSection()
		.selectValuationType(valuationType)
		.setReplacementCost(replacementCost)
		.selectUnitInFireWalls(units)
		.setNumberOfStories(numberOfStories)
		.setSquareFootage(squareFootage)
		.selectFoundationType(foundationType)
		.selectPrimaryHeating(primaryHeating)
		.selectPlumbing(plumbing)
		.selectWiring(wiring)
		.selectElectricalSystem(electricalSystem)
		.selectConditionOfRoof(conditionOfRoof)
		.secondaryHeatingSystem.clickYesOrNo(false);
		app.homeDetails.distanceToCoast
		.toggleRadio();
		app.homeDetails.screenEnclosureOnPremises
		.clickYesOrNo(true);
		app.homeDetails.inTheWindPool.clickYesOrNo(false);

		assertEquals(scho3.app.homeDetails.getYearBuiltReadOnly(), yearBuilt);
		assertEquals(scho3.app.homeDetails.getConstructionTypeReadOnly(), constructionType);
		assertEquals(scho3.app.homeDetails.distanceToFireHydrant.getDistanceToFireHydrantReadOnly(), "Over 1000ft");

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.showSection()
		.lockedFence.clickYesOrNo(true);
		app.dwellingProtectionAndSurcharges.burglarBars.clickYesOrNo(false);

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	public void newNCHO3QuoteDetails()
	{
		String producerCode = "523-23-21388 Acentria, Inc. (MAIN)",
				riskState = "North Carolina",
				policyType = "Homeowners (HO3)",
				areThereAnyOtherPoliciesWithFrontline = "No",
				firstName = "testFirstName",
				lastName = "testLastName",
				locationAddress1 = "104 Baymeade Ct",
				city = "Jacksonville",
				zipCode = "28546";
		nq
		.selectProducerCode("-- Choose Producer Code--")
		.selectProducerCode(producerCode)
		.selectRiskState(riskState)
		.selectPolicyType(policyType)
		.selectPolicyWriter(policyWriter)
		.setEffectiveDate(effectiveDate)
//		.setAreThereAnyOtherPoliciesWithFrontline(areThereAnyOtherPoliciesWithFrontline)
		.setFirstName(firstName)
		.setLastName(lastName)
		.setLocationAddress1(locationAddress1)
		.setCity(city)
		.setZipCode(zipCode);

		assertEquals(nq.getProducerCode(), producerCode);
		assertEquals(nq.getRiskState(), riskState);
		assertEquals(nq.getPolicyType(), policyType);
		assertEquals(nq.getPolicyWriter(), policyWriter);
		assertEquals(nq.getEffectiveDate(), effectiveDate);
//		assertEquals(nq.getAreThereAnyOtherPoliciesWithFrontline(), areThereAnyOtherPoliciesWithFrontline);
		assertEquals(nq.getFirstName(), firstName);
		assertEquals(nq.getLastName(), lastName);
		assertEquals(nq.getLocationAddress1(), locationAddress1);
		assertEquals(nq.getCity(), city);
		assertEquals(nq.getZipCode(), zipCode);

		nq.verifyAddress();
		nq.next();

		String dwellingLimit = "300000",
				yearBuilt = "2005",
				constructionType = "Masonry";

		NCHO3 ncho3 = new NCHO3();

		//Home Details Quote Area
		ncho3.quote.homeDetails.showSection()
		.setYearBuilt(yearBuilt)
		.selectConstructionType(constructionType)
		.distanceToFireHydrant.setOver1000ft();

		assertEquals(ncho3.quote.homeDetails.getYearBuilt(), yearBuilt);
		assertEquals(ncho3.quote.homeDetails.getConstructionType(), constructionType);
		assertEquals(ncho3.quote.homeDetails.distanceToFireHydrant.getDistanceToFireHydrant(), "Over 1000ft");

		//Wind Mitigation Area
		String roofYear = "2005",
				roofType = "Asphalt Shingle",
				roofShape = "Hip";
		ncho3.quote.windMitigation.showSection()
		.setRoofYear(roofYear)
		.selectRoofType(roofType)
		.selectRoofShape(roofShape);

		assertEquals(ncho3.quote.windMitigation.getRoofYear(), roofYear);
		assertEquals(ncho3.quote.windMitigation.getRoofType(), roofType);
		assertEquals(ncho3.quote.windMitigation.getRoofShape(), roofShape);

		//Coverages Area
		ncho3.quote.coverages
		.showSection();
		ncho3.quote.coverages
		.setDwellingLimit(dwellingLimit)
		.clickCalculateQuote()
		.closeValidationMessage();
		ncho3.quote.coverages
		.clickCalculateQuote()
		.windOnlyModalClickOk()
		.continueToApp();

		//Entering Application Phase
		NCHO3SinglePageApplication app = ncho3.continueAsNewCustomer();

		//Applicant Info Area
		String dateOfBirth = "02/04/1990",
				SSN = "123-23-1234",
				homePhone = "407-123-1234";
		app.applicantInfo.showSection()
		.setDateOfBirth(dateOfBirth)
		.setSSN(SSN)
		.setHomePhone(homePhone)
		.setHomePhoneAsPrimary();

		//Home Details Section
		String valuationType = "Appraisal",
				replacementCost = "300000",
				units = "2",
				numberOfStories = "4",
				squareFootage = "2000",
				foundationType = "Open",
				primaryHeating = "Electric",
				plumbing = "PVC",
				wiring = "Copper",
				electricalSystem = "Circuit Breaker",
				conditionOfRoof = "Average";
		app.homeDetails.showSection()
		.selectValuationType(valuationType)
		.setReplacementCost(replacementCost)
		.selectUnitInFireWalls(units)
		.setNumberOfStories(numberOfStories)
		.setSquareFootage(squareFootage)
		.selectFoundationType(foundationType)
		.selectPrimaryHeating(primaryHeating)
		.selectPlumbing(plumbing)
		.selectWiring(wiring)
		.selectElectricalSystem(electricalSystem)
		.selectConditionOfRoof(conditionOfRoof)
		.secondaryHeatingSystem.clickYesOrNo(false);
		app.homeDetails.distanceToCoast
		.toggleRadio();
		app.homeDetails.screenEnclosureOnPremises
		.clickYesOrNo(true);

		assertEquals(ncho3.app.homeDetails.getYearBuiltReadOnly(), yearBuilt);
		assertEquals(ncho3.app.homeDetails.getConstructionTypeReadOnly(), constructionType);
		assertEquals(ncho3.app.homeDetails.distanceToFireHydrant.getDistanceToFireHydrantReadOnly(), "Over 1000ft");

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.showSection()
		.lockedFence.clickYesOrNo(true);
		app.dwellingProtectionAndSurcharges.burglarBars.clickYesOrNo(false);

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	public void newALHO3QuoteDetails()
	{
		String producerCode = "523-23-21388 Acentria, Inc. (MAIN)",
				riskState = "Alabama",
				policyType = "Homeowners (HO3)",
				areThereAnyOtherPoliciesWithFrontline = "No",
				firstName = "testFirstName",
				lastName = "testLastName",
				locationAddress1 = "5 Laredo Dr",
				city = "Saraland",
				county = "Mobile",
		zipCode = "36571";
		nq
		.selectProducerCode("-- Choose Producer Code--")
		.selectProducerCode(producerCode)
		.selectRiskState(riskState)
		.selectPolicyType(policyType)
		.selectPolicyWriter(policyWriter)
		.setEffectiveDate(effectiveDate)
//		.setAreThereAnyOtherPoliciesWithFrontline(areThereAnyOtherPoliciesWithFrontline)
		.setFirstName(firstName)
		.setLastName(lastName)
		.setLocationAddress1(locationAddress1)
		.setCity(city)
		.setCounty(county)
		.setZipCode(zipCode);

		assertEquals(nq.getProducerCode(), producerCode);
		assertEquals(nq.getRiskState(), riskState);
		assertEquals(nq.getPolicyType(), policyType);
		assertEquals(nq.getPolicyWriter(), policyWriter);
		assertEquals(nq.getEffectiveDate(), effectiveDate);
//		assertEquals(nq.getAreThereAnyOtherPoliciesWithFrontline(), areThereAnyOtherPoliciesWithFrontline);
		assertEquals(nq.getFirstName(), firstName);
		assertEquals(nq.getLastName(), lastName);
		assertEquals(nq.getLocationAddress1(), locationAddress1);
		assertEquals(nq.getCity(), city);
		assertEquals(nq.getZipCode(), zipCode);

		nq.verifyAddress();
		nq.next();

		String dwellingLimit = "300000",
				yearBuilt = "2005",
				constructionType = "Masonry";

		ALHO3 alho3 = new ALHO3();

		//Home Details Quote Area
		alho3.quote.homeDetails.showSection()
		.setDwellingLimit(dwellingLimit)
		.setYearBuilt(yearBuilt)
		.selectConstructionType(constructionType)
		.distanceToFireHydrant.setOver1000ft();

		assertEquals(alho3.quote.homeDetails.getDwellingLimit(), dwellingLimit);
		assertEquals(alho3.quote.homeDetails.getYearBuilt(), yearBuilt);
		assertEquals(alho3.quote.homeDetails.getConstructionType(), constructionType);
		assertEquals(alho3.quote.homeDetails.distanceToFireHydrant.getDistanceToFireHydrant(), "Over 1000ft");

		//Wind Mitigation Area
		String roofYear = "2005",
				roofType = "Asphalt Shingle",
				roofShape = "Hip";
		alho3.quote.windMitigation.showSection()
		.setRoofYear(roofYear)
		.selectRoofType(roofType)
		.selectRoofShape(roofShape);

		assertEquals(alho3.quote.windMitigation.getRoofYear(), roofYear);
		assertEquals(alho3.quote.windMitigation.getRoofType(), roofType);
		assertEquals(alho3.quote.windMitigation.getRoofShape(), roofShape);

		//Coverages Area
		alho3.quote.coverages
		.showSection();
		alho3.quote.coverages
		.continueToApp();

		//Entering Application Phase
		ALHO3SinglePageApplication app = alho3.continueAsNewCustomer();

		//Applicant Info Area
		String dateOfBirth = "02/04/1990",
				SSN = "123-23-1234",
				homePhone = "407-123-1234";
		app.applicantInfo.showSection()
		.setDateOfBirth(dateOfBirth)
		.setSSN(SSN)
		.setHomePhone(homePhone)
		.setHomePhoneAsPrimary();

		//Home Details Section
		String valuationType = "Appraisal",
				replacementCost = "300000",
				units = "2",
				numberOfStories = "4",
				squareFootage = "2000",
				foundationType = "Open",
				primaryHeating = "Electric",
				plumbing = "PVC",
				wiring = "Copper",
				electricalSystem = "Circuit Breaker",
				conditionOfRoof = "Average";
		app.homeDetails.showSection()
		.selectValuationType(valuationType)
		.setReplacementCost(replacementCost)
		.selectUnitInFireWalls(units)
		.setNumberOfStories(numberOfStories)
		.setSquareFootage(squareFootage)
		.selectFoundationType(foundationType)
		.selectPrimaryHeating(primaryHeating)
		.selectPlumbing(plumbing)
		.selectWiring(wiring)
		.selectElectricalSystem(electricalSystem)
		.selectConditionOfRoof(conditionOfRoof)
		.secondaryHeatingSystem.clickYesOrNo(false);
		app.homeDetails.distanceToCoast
		.toggleRadio();
		app.homeDetails.screenEnclosureOnPremises
		.clickYesOrNo(true);
		app.homeDetails.inTheWindPool.clickYesOrNo(false);

		assertEquals(alho3.app.homeDetails.getYearBuiltReadOnly(), yearBuilt);
		assertEquals(alho3.app.homeDetails.getConstructionTypeReadOnly(), constructionType);
		assertEquals(alho3.app.homeDetails.distanceToFireHydrant.getDistanceToFireHydrantReadOnly(), "Over 1000ft");

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.showSection()
		.lockedFence.clickYesOrNo(true);
		app.dwellingProtectionAndSurcharges.burglarBars.clickYesOrNo(false);

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}
}