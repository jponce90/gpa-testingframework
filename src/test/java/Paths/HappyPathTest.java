package Paths;

import TestData.HappyPathDataProvider;
import base.BaseTest;
import helpers.LocalDriverManager;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageobjects.ALHO3.ALHO3;
import pageobjects.ALHO3.ALHO3SinglePageApplication;
import pageobjects.FLHO3.FLHO3;
import pageobjects.FLHO3.FLHO3SinglePageApplication;
import pageobjects.FLHO6.FLHO6;
import pageobjects.FLHO6.FLHO6SinglePageApplication;
import pageobjects.NCHO3.NCHO3;
import pageobjects.NCHO3.NCHO3SinglePageApplication;
import pageobjects.NewQuote;
import pageobjects.SCHO3.SCHO3;
import pageobjects.SCHO3.SCHO3SinglePageApplication;
import pageobjects.SCHO6.SCHO6;
import pageobjects.SCHO6.SCHO6SinglePageApplication;


import java.util.LinkedHashMap;

@Test
public class HappyPathTest extends BaseTest
{

	@BeforeMethod
	public void HappyPathBeforeMethod()
	{
		new NewQuote().get();
	}

	@AfterMethod
	public void AfterMethod(ITestResult iTestResult)
	{
		if(!iTestResult.isSuccess())
			takeScreenShot(iTestResult);
		LocalDriverManager.getDriver().quit();
	}

	//region HO3 Happy Paths
	@Test(dataProviderClass = HappyPathDataProvider.class, dataProvider = "FLHO3HappyPath")
	public void FLHO3HappyPathTest(LinkedHashMap<String, String> hashMap)
	{
		//New Quote Area
		NewQuote nq = new NewQuote();
		nq.setNewQuoteFieldsFromHashMap(hashMap).next();
		FLHO3 flho3 = new FLHO3();

		//Home Details Quote Area
		flho3.quote.homeDetails.setHomeDetailsFromHashMap(hashMap);

		//Wind Mitigation Area
		flho3.quote.windMitigation.setWindMitigationFromHashMap(hashMap);

		//Dwelling Protection discounts & Surcharges area
		flho3.quote.dwellingProtectionDiscountsAndSurcharges.setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(hashMap);

		//Coverages Area
		flho3.quote.coverages
//		.showSectionIgnoreValidationWarning();
		.showSection();

		String coverage = hashMap.get("CoveragesContinuetoappfrommostwhatversion");
		switch(coverage)
		{
			case "More Coverage":
				flho3.quote.coverages.clickMoreCoverage();
				break;
			case "Most Popular":
				flho3.quote.coverages.clickMostPopular();
				break;
			case "Less Coverage":
				flho3.quote.coverages.clickLessCoverage();
				break;
			default:
				assert false;
				break;
		}
		flho3.quote.coverages
		.continueToApp();

		//Entering Application Phase
		FLHO3SinglePageApplication app = flho3.continueAsNewCustomer();

		//Applicant Info Area
		app.applicantInfo
		.setAppApplicantInfoFieldsFromHashMap(hashMap);

		//Home Details Section
		app.homeDetails.setAppHomeDetailFieldsFromHashMap(hashMap);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(hashMap);

		//Additional Info Section
		//TODO SECTION

		//Underwriting Questions Section
		//TODO SECTION

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	@Test(dataProviderClass = HappyPathDataProvider.class, dataProvider="ALHO3HappyPath")
	public void ALHO3HappyPathTest(LinkedHashMap<String, String> hashMap)
	{
		//New Quote Area
		NewQuote nq = new NewQuote();
		nq.setNewQuoteFieldsFromHashMap(hashMap).next();
		ALHO3 alho3 = new ALHO3();

		//Home Details Quote Area
		alho3.quote.homeDetails.setHomeDetailsFromHashMap(hashMap);

		//Wind Mitigation Area
		alho3.quote.windMitigation.setWindMitigationFromHashMap(hashMap);

		//Dwelling Protection discounts & Surcharges area
		alho3.quote.dwellingProtectionAndSurcharges.setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(hashMap);

		//Coverages Area
		alho3.quote.coverages
//		.showSectionIgnoreValidationWarning();
		.showSection();
//
		String coverage = hashMap.get("CoveragesContinuetoappfrommostwhatversion");
		switch(coverage)
		{
			case "More Coverage":
				alho3.quote.coverages.clickMoreCoverage();
				break;
			case "Most Popular":
				alho3.quote.coverages.clickMostPopular();
				break;
			case "Less Coverage":
				alho3.quote.coverages.clickLessCoverage();
				break;
			default:
				System.out.println("Attempting to use invalid Coverage Type Hash map provided: '" + coverage +"'");
				assert false;
				break;
		}
		alho3.quote.coverages
		.continueToApp();

		//Entering Application Phase
		ALHO3SinglePageApplication app = alho3.continueAsNewCustomer();

		//Applicant Info Area
		app.applicantInfo
		.setAppApplicantInfoFieldsFromHashMap(hashMap);

		//Home Details Section
		app.homeDetails.setAppHomeDetailFieldsFromHashMap(hashMap);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(hashMap);

		//Additional Info Section
		//TODO SECTION

		//Underwriting Questions Section
		//TODO SECTION

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	@Test(dataProviderClass = HappyPathDataProvider.class, dataProvider="NCHO3HappyPath")
	public void NCHO3HappyPathTest(LinkedHashMap<String, String> hashMap)
	{
		//New Quote Area
		NewQuote nq = new NewQuote();
		nq.setNewQuoteFieldsFromHashMap(hashMap).next();
		NCHO3 ncho3 = new NCHO3();

		//Home Details Quote Area
		ncho3.quote.homeDetails.setHomeDetailsFromHashMap(hashMap);

		//Wind Mitigation Area
		ncho3.quote.windMitigation.setWindMitigationFromHashMap(hashMap);

		//Dwelling Protection discounts & Surcharges area
		ncho3.quote.dwellingProtectionAndSurcharges.setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(hashMap);

		//Coverages Area
		ncho3.quote.coverages
//		.showSectionIgnoreValidationWarning();
		.showSection();
		ncho3.quote.coverages
		.setDwellingLimit(hashMap.get("CoveragesDwellingLimit"))
		.calculateQuote()
		.continueToApp();

		//Entering Application Phase
		NCHO3SinglePageApplication app = ncho3.continueAsNewCustomer();

		//Applicant Info Area
		app.applicantInfo.setAppApplicantInfoFieldsFromHashMap(hashMap);

		//Home Details Section
		app.homeDetails.setAppHomeDetailFieldsFromHashMap(hashMap);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(hashMap);

		//Additional Info Section
		//TODO SECTION

		//Underwriting Questions Section
		//TODO SECTION

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	@Test(dataProviderClass = HappyPathDataProvider.class, dataProvider="SCHO3HappyPath")
	public void SCHO3HappyPathTest(LinkedHashMap<String, String> hashMap)
	{
		//New Quote Area
		NewQuote nq = new NewQuote();
		nq.setNewQuoteFieldsFromHashMap(hashMap).next();
		SCHO3 scho3 = new SCHO3();

		//Home Details Quote Area
		scho3.quote.homeDetails.setHomeDetailsFromHashMap(hashMap);

		//Wind Mitigation Area
		scho3.quote.windMitigation.setWindMitigationFromHashMap(hashMap);

		//Dwelling Protection discounts & Surcharges area
		scho3.quote.dwellingProtectionAndSurcharges.setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(hashMap);

		//Coverages Area
		scho3.quote.coverages
//		.showSectionIgnoreValidationWarning();
		.showSection();

		String coverage = hashMap.get("CoveragesContinuetoappfrommostwhatversion");
		switch(coverage)
		{
			case "More Coverage":
				scho3.quote.coverages.clickMoreCoverage();
				break;
			case "Most Popular":
				scho3.quote.coverages.clickMostPopular();
				break;
			case "Less Coverage":
				scho3.quote.coverages.clickLessCoverage();
				break;
			default:
				System.out.println("Attempting to use invalid Coverage Type Hash map provided: '" + coverage +"'");
				assert false;
				break;
		}
		scho3.quote.coverages
		.continueToApp();

		//Entering Application Phase
		SCHO3SinglePageApplication app = scho3.continueAsNewCustomer();

		//Applicant Info Area
		app.applicantInfo.setAppApplicantInfoFieldsFromHashMap(hashMap);

		//Home Details Section
		app.homeDetails.setAppHomeDetailFieldsFromHashMap(hashMap);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(hashMap);

		//Additional Info Section
		//TODO SECTION

		//Underwriting Questions Section
		//TODO SECTION

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}
	//endregion

	@Test(dataProviderClass = HappyPathDataProvider.class, dataProvider = "FLHO6HappyPath")
	public void FLHO6HappyPathTest(LinkedHashMap<String, String> hashMap)
	{
		//New Quote Area
		NewQuote nq = new NewQuote();
		nq.setNewQuoteFieldsFromHashMap(hashMap).next();
		FLHO6 flho6 = new FLHO6();

		//Home Details Quote Area
		flho6.quote.homeDetails.setHomeDetailsFromHashMap(hashMap);

		//Wind Mitigation Area
		flho6.quote.windMitigation.setWindMitigationFromHashMap(hashMap);

		//Dwelling Protection discounts & Surcharges area
		flho6.quote.dwellingProtectionDiscountsAndSurcharges.setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(hashMap);

		//Coverages Area
		flho6.quote.coverages
//		.showSectionIgnoreValidationWarning();
		.showSection();

		String coverage = hashMap.get("CoveragesContinuetoappfrommostwhatversion");
		switch(coverage)
		{
			case "More Coverage":
				flho6.quote.coverages.clickMoreCoverage();
				break;
			case "Most Popular":
				flho6.quote.coverages.clickMostPopular();
				break;
			case "Less Coverage":
				flho6.quote.coverages.clickLessCoverage();
				break;
			default:
				assert false;
				break;
		}
		flho6.quote.coverages
		.continueToApp();

		//Entering Application Phase
		FLHO6SinglePageApplication app = flho6.continueAsNewCustomer();

		//Applicant Info Area
		app.applicantInfo
		.setAppApplicantInfoFieldsFromHashMap(hashMap);

		//Home Details Section
		app.homeDetails.setAppHomeDetailFieldsFromHashMap(hashMap);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(hashMap);

		//Additional Info Section
		//TODO SECTION

		//Underwriting Questions Section
		//TODO SECTION

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}

	@Test(dataProviderClass = HappyPathDataProvider.class, dataProvider = "SCHO6HappyPath")
	public void SCHO6HappyPathTest(LinkedHashMap<String, String> hashMap)
	{
		//New Quote Area
		NewQuote nq = new NewQuote();
		nq.setNewQuoteFieldsFromHashMap(hashMap).next();
		SCHO6 scho6 = new SCHO6();

		//Home Details Quote Area
		scho6.quote.homeDetails.setHomeDetailsFromHashMap(hashMap);

		//Wind Mitigation Area
		scho6.quote.windMitigation.setWindMitigationFromHashMap(hashMap);

		//Dwelling Protection discounts & Surcharges area
		scho6.quote.dwellingProtectionDiscountsAndSurcharges.setDwellingProtectionDiscountsAndSurchargesFieldsFromHashMap(hashMap);

		//Coverages Area
		scho6.quote.coverages
//		.showSectionIgnoreValidationWarning();
		.showSection();

		String coverage = hashMap.get("CoveragesContinuetoappfrommostwhatversion");
		switch(coverage)
		{
			case "More Coverage":
				scho6.quote.coverages.clickMoreCoverage();
				break;
			case "Most Popular":
				scho6.quote.coverages.clickMostPopular();
				break;
			case "Less Coverage":
				scho6.quote.coverages.clickLessCoverage();
				break;
			default:
				assert false;
				break;
		}
		scho6.quote.coverages
		.continueToApp();

		//Entering Application Phase
		SCHO6SinglePageApplication app = scho6.continueAsNewCustomer();

		//Applicant Info Area
		app.applicantInfo
		.setAppApplicantInfoFieldsFromHashMap(hashMap);

		//Home Details Section
		app.homeDetails.setAppHomeDetailFieldsFromHashMap(hashMap);

		//Dwelling and Surcharges Area
		app.dwellingProtectionAndSurcharges.setAppDwellingProtectionDiscountsAndSurchargesFromHashMap(hashMap);

		//Additional Info Section
		//TODO SECTION

		//Underwriting Questions Section
		//TODO SECTION

		//Loss History Area
		app.lossHistory.showSection()
		.orderLossReport();

		//Binding Policy
		app.bindPolicy.bindPolicy();
	}
}