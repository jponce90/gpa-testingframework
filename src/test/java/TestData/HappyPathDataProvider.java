package TestData;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.List;


public class HappyPathDataProvider
{

	@DataProvider(parallel = false)
	public static Object[][] FLHO3HappyPath()
	{
		Object[][] objects = null;
		List<LinkedHashMap<String, String>> testData = new ArrayList<>();
		try
		{
			System.out.println(new File(".").getAbsolutePath());
			String excelFilePath = "src/test/java/TestData/FLHO3TestData.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			Iterator<Cell> cellIterator = iterator.next().cellIterator();
			int numPaths = 0;
			while(cellIterator.hasNext())
			{
				cellIterator.next();
				numPaths++;
			}
			System.out.println(numPaths + " Test Cases were found in this file");
			objects = new Object[numPaths][];
			iterator.next();
			for(int i = 0 ; i<numPaths ; i++)
				testData.add(new LinkedHashMap<>());
			while(testData.get(0).size() < 147)
			{
				Row nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				cellIterator.next();
				String key = cellIterator.next().getStringCellValue().replaceAll(" ", "");
				key = key+cellIterator.next().getStringCellValue().replaceAll(" ", "").replace("?", "");
				int currentPath = 0;
				while(cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					testData.get(currentPath%numPaths).put(key, cell.getStringCellValue());
					currentPath++;
				}
			}

			workbook.close();
			inputStream.close();
			int objectCounter = 0;
			for(LinkedHashMap<String, String> hashMap : testData)
				objects[objectCounter++] = new Object[]{(hashMap)};
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return objects;
	}

	@DataProvider(parallel = false)
	public static Object[][] ALHO3HappyPath()
	{
		Object[][] objects = null;
		List<LinkedHashMap<String, String>> testData = new ArrayList<>();
		try
		{
			System.out.println(new File(".").getAbsolutePath());
			String excelFilePath = "src/test/java/TestData/ALHO3TestData.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			Iterator<Cell> cellIterator = iterator.next().cellIterator();
			cellIterator.next();
			int numPaths = 0;
			while(cellIterator.hasNext())
			{
				cellIterator.next();
				numPaths++;
			}
			System.out.println(numPaths + " Test Cases were found in this file");
			objects = new Object[numPaths][];
			iterator.next();
			for(int i = 0 ; i<numPaths ; i++)
				testData.add(new LinkedHashMap<>());
			while(testData.get(0).size() < 147)
			{
				Row nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				cellIterator.next();
				String key = cellIterator.next().getStringCellValue().replaceAll(" ", "");
				key = key+cellIterator.next().getStringCellValue().replaceAll(" ", "").replace("?", "");
				int currentPath = 0;
				while(cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					testData.get(currentPath%numPaths).put(key, cell.getStringCellValue().replaceAll("\n", ""));
					currentPath++;
				}
			}

			workbook.close();
			inputStream.close();
			int objectCounter = 0;
			for(LinkedHashMap<String, String> hashMap : testData)
			{
				objects[objectCounter++] = new Object[]{(hashMap)};
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return objects;
	}

	@DataProvider(parallel = false)
	public static Object[][] NCHO3HappyPath()
	{
		Object[][] objects = null;
		List<LinkedHashMap<String, String>> testData = new ArrayList<>();
		try
		{
			System.out.println(new File(".").getAbsolutePath());
			String excelFilePath = "src/test/java/TestData/NCHO3TestData.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			Iterator<Cell> cellIterator = iterator.next().cellIterator();
			cellIterator.next();
			int numPaths = 0;
			while(cellIterator.hasNext())
			{
				cellIterator.next();
				numPaths++;
			}
			System.out.println(numPaths + " Test Cases were found in this file");
			objects = new Object[numPaths][];
			iterator.next();
			for(int i = 0 ; i<numPaths ; i++)
				testData.add(new LinkedHashMap<>());
			while(testData.get(0).size() < 147)
			{
				Row nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				cellIterator.next();
				String key = cellIterator.next().getStringCellValue().replaceAll(" ", "");
				key = key+cellIterator.next().getStringCellValue().replaceAll(" ", "").replace("?", "");
				int currentPath = 0;
				while(cellIterator.hasNext() && currentPath < numPaths)
				{
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					testData.get(currentPath%numPaths).put(key, cell.getStringCellValue().replaceAll("\n", ""));
					currentPath++;
				}
			}

			workbook.close();
			inputStream.close();
			int objectCounter = 0;
			for(LinkedHashMap<String, String> hashMap : testData)
			{
				objects[objectCounter++] = new Object[]{(hashMap)};
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return objects;
	}

	@DataProvider(parallel = false)
	public static Object[][] SCHO3HappyPath()
	{
		Object[][] objects = null;
		List<LinkedHashMap<String, String>> testData = new ArrayList<>();
		try
		{
			System.out.println(new File(".").getAbsolutePath());
			String excelFilePath = "src/test/java/TestData/SCHO3TestData.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			Iterator<Cell> cellIterator = iterator.next().cellIterator();
			int numPaths = 0;
			while(cellIterator.hasNext())
			{
				Cell cell = cellIterator.next();
				numPaths++;
			}
			System.out.println(numPaths + " Test Cases were found in this file");
			objects = new Object[numPaths][];
			iterator.next();
			for(int i = 0 ; i<numPaths ; i++)
				testData.add(new LinkedHashMap<>());
			while(testData.get(0).size() < 142)
			{
				Row nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				cellIterator.next();
				String key = cellIterator.next().getStringCellValue().replaceAll(" ", "");
				key = key+cellIterator.next().getStringCellValue().replaceAll(" ", "").replace("?", "");
				int currentPath = 0;
				while(cellIterator.hasNext() && currentPath < numPaths)
				{
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					testData.get(currentPath%numPaths).put(key, cell.getStringCellValue().replaceAll("\n", ""));
					currentPath++;
				}
			}

			workbook.close();
			inputStream.close();
			int objectCounter = 0;
			for(LinkedHashMap<String, String> hashMap : testData)
			{
				objects[objectCounter++] = new Object[]{(hashMap)};
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return objects;
	}

	@DataProvider(parallel = false)
	public static Object[][] FLHO6HappyPath()
	{
		Object[][] objects = null;
		List<LinkedHashMap<String, String>> testData = new ArrayList<>();
		try
		{
			System.out.println(new File(".").getAbsolutePath());
			String excelFilePath = "src/test/java/TestData/FLHO6TestData.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			Iterator<Cell> cellIterator = iterator.next().cellIterator();
			int numPaths = 0;
			while(cellIterator.hasNext())
			{
				cellIterator.next();
				numPaths++;
			}
			System.out.println(numPaths + " Test Cases were found in this file");
			objects = new Object[numPaths][];
			iterator.next();
			for(int i = 0 ; i<numPaths ; i++)
				testData.add(new LinkedHashMap<>());
			while(testData.get(0).size() < 147)
			{
				Row nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				cellIterator.next();
				String key = cellIterator.next().getStringCellValue().replaceAll(" ", "");
				key = key+cellIterator.next().getStringCellValue().replaceAll(" ", "").replace("?", "");
				int currentPath = 0;
				while(cellIterator.hasNext() && currentPath < numPaths)
				{
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					testData.get(currentPath%numPaths).put(key, cell.getStringCellValue());
					currentPath++;
				}
			}

			workbook.close();
			inputStream.close();
			int objectCounter = 0;
			for(LinkedHashMap<String, String> hashMap : testData)
				objects[objectCounter++] = new Object[]{(hashMap)};
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return objects;
	}

	@DataProvider(parallel = false)
	public static Object[][] SCHO6HappyPath()
	{
		Object[][] objects = null;
		List<LinkedHashMap<String, String>> testData = new ArrayList<>();
		try
		{
			System.out.println(new File(".").getAbsolutePath());
			String excelFilePath = "src/test/java/TestData/SCHO6TestData.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			Iterator<Cell> cellIterator = iterator.next().cellIterator();
			int numPaths = 0;
			while(cellIterator.hasNext())
			{
				cellIterator.next();
				numPaths++;
			}
			System.out.println(numPaths + " Test Cases were found in this file");
			objects = new Object[numPaths][];
			iterator.next();
			for(int i = 0 ; i<numPaths ; i++)
				testData.add(new LinkedHashMap<>());
			while(testData.get(0).size() < 147)
			{
				Row nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				cellIterator.next();
				String key = cellIterator.next().getStringCellValue().replaceAll(" ", "");
				key = key+cellIterator.next().getStringCellValue().replaceAll(" ", "").replace("?", "");
				int currentPath = 0;
				while(cellIterator.hasNext() && currentPath < numPaths)
				{
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					testData.get(currentPath%numPaths).put(key, cell.getStringCellValue());
					currentPath++;
				}
			}

			workbook.close();
			inputStream.close();
			int objectCounter = 0;
			for(LinkedHashMap<String, String> hashMap : testData)
				objects[objectCounter++] = new Object[]{(hashMap)};
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return objects;
	}
}