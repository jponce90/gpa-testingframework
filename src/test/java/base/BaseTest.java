package base;

import helpers.LocalDriverManager;
import helpers.SessionInfo;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.xml.XmlTest;
import pageobjects.NewQuote;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public abstract class BaseTest
{
	protected static String screenShotDirectory = "src/test/resources/ScreenShots/",accountNumber;
	private File screenShotFolder = new File(screenShotDirectory);
	private static SessionInfo sessionInfo;
	private Boolean runOnGrid;
	public final Logger logger = LoggerFactory.getLogger(getClass());
	private String lastLoggedMessage;

	@Parameters({"environment", "runOnGrid", "threads"})
	@BeforeSuite
	public void beforeSuite(XmlTest xml, @Optional("127") String environment, @Optional("false") Boolean runOnGrid, @Optional("10") int threads)
	{
		xml.getSuite().setThreadCount(threads);
		FileUtils.deleteQuietly(screenShotFolder);
		screenShotFolder.mkdir();
		sessionInfo = new SessionInfo(environment, setCapabilities(), setGridHub());
		this.runOnGrid = runOnGrid;
		assert sessionInfo.capabilities != null;
		assert sessionInfo.gridHub != null;
	}


	@BeforeMethod
	public void BeforeMethod()
	{
		setupDriver();
	}

	protected URL setGridHub()
	{
		URL gridHub = null;
		try
		{
			gridHub = new URL("http://172.16.31.94:4444/wd/hub");
//			gridHub = new URL("http://localhost:4444/wd/hub");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return gridHub;
	}

	protected void takeScreenshot(WebDriver driver, String directory, String methodName)
	{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try
			{
				FileUtils.copyFile(scrFile, new File(directory + methodName + ".png"));
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
	}

	protected DesiredCapabilities setCapabilities()
	{
		DesiredCapabilities capabilities;
		capabilities = DesiredCapabilities.firefox();
//		capabilities = DesiredCapabilities.internetExplorer();
//		capabilities.setCapability("enablePersistentHover", false);
//		capabilities.setCapability("ie.ensureCleanSession", true);
//		capabilities.setCapability("ignoreProtectedModeSettings", true);
//		capabilities.setCapability("seleniumProtocol", "WebDriver");
//		capabilities.setCapability("requireWindowFocus", true);
		return capabilities;
	}

	protected WebDriver setupDriver()
	{
		WebDriver driver = null;
		if(runOnGrid)
		{
			driver = new RemoteWebDriver(sessionInfo.gridHub, sessionInfo.capabilities);
		}
		else{
			driver = new FirefoxDriver(sessionInfo.capabilities);
		}
		driver.manage().window().maximize();
		LocalDriverManager.setWebDriver(driver, sessionInfo);
		return driver;
	}

	protected void failureBehavior(WebDriver driver, String directory, String methodName)
	{
		takeScreenshot(driver, directory, methodName);
	}

	public void log(String message) {
		logger.info(message);
		lastLoggedMessage = message;
	}

	public String getLastLoggedMessage() {
		return lastLoggedMessage;
	}

	protected String takeScreenShot(ITestResult result)
	{
		WebDriver driver = LocalDriverManager.getDriver();
		File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		String fileName = screenShotDirectory +
				"TestScreenShot" +
				"_" + ((RemoteWebDriver) driver).getCapabilities().getBrowserName() +
				"_" +  result.getMethod().getMethodName()+
				"_" + new DateTime().toString("k-mm-ss");

		try
		{
			FileUtils.moveFile(screenShot, new File(fileName + ".jpg"));
		}
		catch(FileExistsException e)
		{
			try
			{
				FileUtils.moveFile(screenShot, new File(fileName + new DateTime().toString("S") + ".jpg"));
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return fileName.substring(fileName.lastIndexOf("/") + 1);
	}

}